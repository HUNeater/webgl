part of web;

class FragmentShader {
    String code;

    FragmentShader() {
        code = """
precision highp float;

varying vec3 normal;
varying vec3 fragPos;
varying vec2 texCoord;

struct Material {
	vec3 color;
	vec3 ambient;
	vec3 specular;
	float shininess;
};

struct Light {
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
	vec3 pos;

	float constant;
	float linear;
	float quadratic;
};

uniform sampler2D texture;
uniform Material mat;
uniform Light light;
uniform vec3 viewPos;
uniform bool useTexture;
uniform bool useFlatShading;

void main(void) {
    if (useTexture) {
        vec4 tex = texture2D(texture, texCoord);
        if (tex.w < 1.0)
            discard;

        gl_FragColor = tex;
    } else {
        gl_FragColor = vec4(mat.color.x, mat.color.y, mat.color.z, 1.0);
    }
}
        """;
    }
}