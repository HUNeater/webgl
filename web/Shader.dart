part of web;

class ShaderProgram {
    bool using = false;
    Program program;
    Shader vx, fr;

    ShaderProgram(VertexShader vertexShader, FragmentShader fragmentShader) {
        program = gl.createProgram();

        vx = createShader(vertexShader.code, VERTEX_SHADER);
        fr = createShader(fragmentShader.code, FRAGMENT_SHADER);

        gl.linkProgram(program);
        if (!gl.getProgramParameter(program, LINK_STATUS))
            throw new Exception(gl.getProgramInfoLog(program));

        gl.validateProgram(program);
        if (!gl.getProgramParameter(program, VALIDATE_STATUS))
            throw new Exception(gl.getProgramInfoLog(program));
    }

    Shader createShader(String code, int type) {
        Shader shader = gl.createShader(type);
        if (shader == null)
            throw new Exception(gl.getShaderInfoLog(shader));

        gl.shaderSource(shader, code);
        gl.compileShader(shader);
        if (!gl.getShaderParameter(shader, COMPILE_STATUS))
            throw new Exception(gl.getShaderInfoLog(shader));

        gl.attachShader(program, shader);

        return shader;
    }

    void use() {
        if (!using) {
            gl.useProgram(program);
            using = true;
        }
    }

    void unuse() {
        if (using) {
            gl.useProgram(null);
            using = false;
        }
    }

    void cleanup() {
        unuse();

        if (program != null) {
            if (vx != null) {
                gl.detachShader(program, vx);
                gl.deleteShader(vx);
            }

            if (fr != null) {
                gl.detachShader(program, fr);
                gl.deleteShader(vx);
            }

            gl.deleteProgram(program);
        }
    }
}