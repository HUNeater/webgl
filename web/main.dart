library web;

import 'dart:html';
import 'dart:math';
import 'dart:typed_data';
import 'dart:web_gl';

import 'package:vector_math/vector_math_64.dart';

part 'FragmentShader.dart';
part 'Objects/Base.dart';
part 'Objects/Camera.dart';
part 'Objects/Object.dart';
part 'Objects/Plane.dart';
part 'Shader.dart';
part 'VertexShader.dart';

var canvas, title;
RenderingContext gl;
ShaderProgram objectShader;
List<GLObject> objects;
Camera cam;

void main() {
    initWindow();
    initGL();
    initGame();

    resize();
    window.animationFrame.then(loop);
}

void initWindow() {
    canvas = querySelector("#webgl");
    if (canvas == null)
        throw new Exception("Failed to find canvas");

    title = querySelector("#title");
    if (title == null)
        throw new Exception("Failed to find title");

    window.onResize.listen(onResize);
}

void initGL() {
    gl = canvas.getContext3d();
    if (gl == null)
        throw new Exception("Failed to initializte OpenGL");

    gl.clearColor(0, 0, 0, 1);
    gl.enable(DEPTH_TEST);
    gl.activeTexture(TEXTURE0);
}

void initGame() {
    objectShader = new ShaderProgram(new VertexShader(), new FragmentShader());

    cam = new Camera(0, 0, 0);
    objects = new List<GLObject>();

    ImageElement floorTex = new ImageElement(src: "res/tex.png");
    floorTex.onLoad.listen((e) {
        Plane floor = new Plane(5, -12, 5, floorTex);
        floor.setScale(20, 20, 1);
        floor.setRotation(90, 0, 0);
        floor.isStatic = true;

        objects.add(floor);
    });

    ImageElement grassTex = new ImageElement(src: "res/grass.png");
    grassTex.onLoad.listen((e) {
        var rng = new Random();

        for (int i = 0; i < 30; i++) {
            var x = rng.nextInt(20) - 10;
            var z = rng.nextInt(20) - 10;

            Plane grass = new Plane(x, -2, z, grassTex);
            grass.setRotation(0, 0, -90);

            objects.add(grass);
        }
    });
}

num fpsTimer = 0;
num lastTime = 0;

void loop(time) {
    num deltaTime = (time - lastTime) / 1000;
    lastTime = time;

    fpsTimer += deltaTime;

    // fps calculating
    if (fpsTimer > 1) {
        int fps = (1 / deltaTime).round();
        title.text = "WebPage ${fps} fps";

        fpsTimer = 0;
    }

    tick(time, deltaTime);
    render();

    window.animationFrame.then(loop);
}

void tick(num time, num delta) {
    cam.update(time, delta);
}

void render() {
    gl.clear(COLOR_BUFFER_BIT | DEPTH_BUFFER_BIT);
    objects.forEach((GLObject) => GLObject.render());
}

void resize() {
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;

    gl.viewport(0, 0, window.innerWidth, window.innerHeight);
    cam.resize(window.innerWidth, window.innerHeight);
}

void onResize(Event e) {
    resize();
}