part of web;

class Plane extends GLObject {
    Plane([num x, num y, num z, ImageElement tex]) : super(x, y, z, tex) {
        Float32List verts = new Float32List.fromList([
            0.5, 0.5, 0.0, 0.0, 0.0, -1.0, 1.0, 1.0,
            -0.5, 0.5, 0.0, 0.0, 0.0, -1.0, 1.0, 0.0,
            -0.5, -0.5, 0.0, 0.0, 0.0, -1.0, 0.0, 0.0,
            0.5, -0.5, 0.0, 0.0, 0.0, -1.0, 0.0, 1.0
        ]);

        Uint16List inds = new Uint16List.fromList([
            0, 1, 2,
            0, 2, 3
        ]);

        var pos = gl.getAttribLocation(objectShader.program, "pos");
        var norm = gl.getAttribLocation(objectShader.program, "norm");
        var coord = gl.getAttribLocation(objectShader.program, "coord");

        gl.bindBuffer(ARRAY_BUFFER, VBO);
        gl.bufferData(ARRAY_BUFFER, verts, STATIC_DRAW);

        gl.bindBuffer(ELEMENT_ARRAY_BUFFER, EBO);
        gl.bufferData(ELEMENT_ARRAY_BUFFER, inds, STATIC_DRAW);

        gl.vertexAttribPointer(pos, 3, FLOAT, false, 8 * 4, 0);
        gl.enableVertexAttribArray(pos);

        gl.vertexAttribPointer(norm, 3, FLOAT, false, 8 * 4, 12);
        gl.enableVertexAttribArray(norm);

        gl.vertexAttribPointer(coord, 2, FLOAT, false, 8 * 4, 24);
        gl.enableVertexAttribArray(coord);
    }

    @override
    void draw() {
        gl.drawElements(TRIANGLES, 6, UNSIGNED_SHORT, 0);
//        gl.drawArrays(TRIANGLES, 0, 3);
    }
}