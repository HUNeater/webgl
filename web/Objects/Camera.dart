part of web;

class Camera extends GLObject {
    Vector3 upVec;
    Vector3 dirVec;
    Matrix4 viewMatrix, projectionMatrix;
    num width, height;
    num fov = 45;

    Camera([num x, num y, num z]) : super(x, y, z) {
        upVec = new Vector3(0.0, 1.0, 0.0);
        dirVec = new Vector3(0.0, 0.0, 0.0);

        isVisible = false;
    }

    void update(num time, num delta) {
        num rad = 10;
        num speed = .001;

        worldLocation.x += sin(time * speed) * rad * delta;
        worldLocation.z += cos(time * speed) * rad * delta;

//        dirVec = getAbsoluteLocation();
//        dirVec.x += sin(time * .0005) * delta;
//        dirVec.z += cos(time * .0005) * delta;

        viewMatrix = makeViewMatrix(getAbsoluteLocation(), dirVec, upVec);

        uniform(objectShader, "view", viewMatrix);
        uniform(objectShader, "viewPos", getAbsoluteLocation());
    }

    void resize(num width, num height) {
        this.width = width;
        this.height = height;

        projectionMatrix = makePerspectiveMatrix(radians(fov.toDouble()),
            width.toDouble() / height.toDouble(),
            0.1, 100.0);

        uniform(objectShader, "projection", projectionMatrix);
    }

    @override
    void draw() {

    }
}