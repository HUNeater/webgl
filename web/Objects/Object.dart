part of web;

abstract class GLObject extends Base {
    Vector3 worldLocation;
    Vector3 relativeLocation;
    Vector3 color, ambientColor;
    Vector3 scale;
    bool isVisible, showCollision, isStatic;
    GLObject collidable;
    Vector3 rotation;
    Matrix4 modelMatrix;
    Texture TBO;
    Buffer VBO, EBO;

    void draw();

    // Defaults
    GLObject([num x, num y, num z, ImageElement tex]) {
        color = new Vector3.all(0.0);
        ambientColor = new Vector3.all(0.0);

        worldLocation = new Vector3.all(0.0);
        relativeLocation =new Vector3.all(0.0);

        scale = new Vector3.all(1.0);
        rotation = new Vector3.all(0.0);

        modelMatrix = new Matrix4.identity();

        isVisible = true;
        isStatic = false;
        showCollision = false;

        setColor(1.0, 0.0, 0.0);

        if (x != null && y != null && z != null) {
            worldLocation.x = x.toDouble();
            worldLocation.y = y.toDouble();
            worldLocation.z = z.toDouble();
        }

        if (tex != null) {
            TBO = gl.createTexture();

            gl.bindTexture(TEXTURE_2D, TBO);

            gl.texParameteri(TEXTURE_2D, TEXTURE_MIN_FILTER, NEAREST);
            gl.texParameteri(TEXTURE_2D, TEXTURE_MAG_FILTER, NEAREST);

            gl.texImage2D(TEXTURE_2D, 0, RGBA, RGBA, UNSIGNED_BYTE, tex);
            gl.generateMipmap(TEXTURE_2D);

            gl.bindTexture(TEXTURE_2D, null);
        }

        VBO = gl.createBuffer();
        EBO = gl.createBuffer();
    }

    void render() {
        tick();

        gl.bindBuffer(ARRAY_BUFFER, VBO);
        gl.bindBuffer(ELEMENT_ARRAY_BUFFER, EBO);
        if (TBO != null) gl.bindTexture(TEXTURE_2D, TBO);
        if (isVisible) draw();
        if (TBO != null) gl.bindTexture(TEXTURE_2D, null);
        gl.bindBuffer(ARRAY_BUFFER, null);
        gl.bindBuffer(ELEMENT_ARRAY_BUFFER, null);
    }

    void tick() {
        if (collidable != null) {
            collidable.worldLocation = worldLocation;

            if (showCollision)
                collidable.render();
        }

        if (TBO != null)
            uniform(objectShader, "useTexture", true);
        else
            uniform(objectShader, "useTexture", false);

        if (!isStatic) {
            Vector3 target = cam.getAbsoluteLocation() - getAbsoluteLocation();
            target.normalize();

            num dot = dot3(target, getForwardVector());
            num angle = acos(dot);

            num x = degrees(getRotation().x);
            num y = degrees(angle);
            num z = degrees(getRotation().z);

            setRotation(x, y, z);
        }

        modelMatrix.setIdentity();
        modelMatrix.translate(worldLocation);
        modelMatrix.translate(relativeLocation);
        modelMatrix.translate(0.5 * scale.x, 0.5 * scale.y, 0.0);
        modelMatrix.rotateX(rotation.x);
        modelMatrix.rotateY(rotation.y);
        modelMatrix.rotateZ(rotation.z);
        modelMatrix.translate(-0.5 * scale.x, -0.5 * scale.y, 0.0);
        modelMatrix.scale(scale);

        uniform(objectShader, "model", modelMatrix);
        uniform(objectShader, "mat.color", color);
        uniform(objectShader, "mat.ambient", ambientColor);
        uniform(objectShader, "mat.specular", 0.5, 0.5, 0.5);
        uniform(objectShader, "mat.shininess", 32);
    }

    void setColor(num x, num y, num z) {
        color.x = x;
        color.y = y;
        color.z = z;

        ambientColor = color;
        ambientColor *= 0.5;
    }

    void setScale(num x, num y, num z) {
        scale.x = x.toDouble();
        scale.y = y.toDouble();
        scale.z = z.toDouble();
    }

    void setRotation(num x, num y, num z) {
        rotation.x = radians(x);
        rotation.y = radians(y);
        rotation.z = radians(z);
    }

    Vector3 getAbsoluteLocation() {
        return worldLocation + relativeLocation;
    }

    Vector3 getForwardVector() {
        Vector3 vec = new Vector3.all(0.0);
        vec.x = modelMatrix[8];
        vec.y = modelMatrix[9];
        vec.z = modelMatrix[10];

        return vec;
    }

    Vector3 getRotation() {
        return rotation;
    }
}