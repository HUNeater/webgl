part of web;

class Base {
    void uniform(ShaderProgram shader, String tex, var a, [num b, num c]) {
        var loc = gl.getUniformLocation(shader.program, tex);
        if (loc != -1) {
            shader.use();

            // Vector3
            if (a is Vector3) {
                gl.uniform3f(loc, a.x, a.y, a.z);

                // boolean
            } else if (a is bool) {
                if (a == true)
                    gl.uniform1f(loc, 1);
                else
                    gl.uniform1f(loc, 0);

                // double
            } else if (a is num) {
                if (b != null && c != null) {
                    gl.uniform3f(loc, a, b, c);
                } else {
                    gl.uniform1f(loc, a);
                }

                // Matrix4
            } else if (a is Matrix4) {
                List<num> array = new Float32List(16);
                a.copyIntoArray(array);

                gl.uniformMatrix4fv(loc, false, array);

                // error
            } else {
                throw new Exception("Unimplemented uniform type " + a);
            }
        }
    }
}