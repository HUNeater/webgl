part of web;

class VertexShader {
    String code;

    VertexShader() {
        code = """
precision highp float;

attribute vec3 pos;
attribute vec3 norm;
attribute vec2 coord;

uniform mat4 model;
uniform mat4 projection;
uniform mat4 view;

varying vec3 normal;
varying vec3 fragPos;
varying vec2 texCoord;

highp mat4 transpose(in highp mat4 inMatrix);
highp mat4 inverse(in highp mat4 inMatrix);

void main(void) {
	gl_Position = projection * view * model * vec4(pos, 1.0);

	fragPos = vec3(model * vec4(pos, 1.0));
	normal = mat3(transpose(inverse(model))) * norm;
	texCoord = coord;
}

highp mat4 transpose(in highp mat4 inMatrix) {
    highp vec4 i0 = inMatrix[0];
    highp vec4 i1 = inMatrix[1];
    highp vec4 i2 = inMatrix[2];
    highp vec4 i3 = inMatrix[3];

    highp mat4 outMatrix = mat4(
                 vec4(i0.x, i1.x, i2.x, i3.x),
                 vec4(i0.y, i1.y, i2.y, i3.y),
                 vec4(i0.z, i1.z, i2.z, i3.z),
                 vec4(i0.w, i1.w, i2.w, i3.w)
                 );

    return outMatrix;
}

highp mat4 inverse(in highp mat4 inMatrix) {
    highp vec4 i0 = inMatrix[0];
    highp vec4 i1 = inMatrix[1];
    highp vec4 i2 = inMatrix[2];
    highp vec4 i3 = inMatrix[3];

    highp mat4 outMatrix = mat4(
                     vec4(i0.x, i1.x, i2.x, i3.x),
                     vec4(i0.y, i1.y, i2.y, i3.y),
                     vec4(i0.z, i1.z, i2.z, i3.z),
                     vec4(i0.w, i1.w, i2.w, i3.w)
                     );

    return outMatrix;
}
        """;
    }
}