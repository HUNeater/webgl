(function(){var supportsDirectProtoAccess=function(){var z=function(){}
z.prototype={p:{}}
var y=new z()
if(!(y.__proto__&&y.__proto__.p===z.prototype.p))return false
try{if(typeof navigator!="undefined"&&typeof navigator.userAgent=="string"&&navigator.userAgent.indexOf("Chrome/")>=0)return true
if(typeof version=="function"&&version.length==0){var x=version()
if(/^\d+\.\d+\.\d+\.\d+$/.test(x))return true}}catch(w){}return false}()
function map(a){a=Object.create(null)
a.x=0
delete a.x
return a}var A=map()
var B=map()
var C=map()
var D=map()
var E=map()
var F=map()
var G=map()
var H=map()
var J=map()
var K=map()
var L=map()
var M=map()
var N=map()
var O=map()
var P=map()
var Q=map()
var R=map()
var S=map()
var T=map()
var U=map()
var V=map()
var W=map()
var X=map()
var Y=map()
var Z=map()
function I(){}init()
function setupProgram(a,b){"use strict"
function generateAccessor(a9,b0,b1){var g=a9.split("-")
var f=g[0]
var e=f.length
var d=f.charCodeAt(e-1)
var c
if(g.length>1)c=true
else c=false
d=d>=60&&d<=64?d-59:d>=123&&d<=126?d-117:d>=37&&d<=43?d-27:0
if(d){var a0=d&3
var a1=d>>2
var a2=f=f.substring(0,e-1)
var a3=f.indexOf(":")
if(a3>0){a2=f.substring(0,a3)
f=f.substring(a3+1)}if(a0){var a4=a0&2?"r":""
var a5=a0&1?"this":"r"
var a6="return "+a5+"."+f
var a7=b1+".prototype.g"+a2+"="
var a8="function("+a4+"){"+a6+"}"
if(c)b0.push(a7+"$reflectable("+a8+");\n")
else b0.push(a7+a8+";\n")}if(a1){var a4=a1&2?"r,v":"v"
var a5=a1&1?"this":"r"
var a6=a5+"."+f+"=v"
var a7=b1+".prototype.s"+a2+"="
var a8="function("+a4+"){"+a6+"}"
if(c)b0.push(a7+"$reflectable("+a8+");\n")
else b0.push(a7+a8+";\n")}}return f}function defineClass(a2,a3){var g=[]
var f="function "+a2+"("
var e=""
var d=""
for(var c=0;c<a3.length;c++){if(c!=0)f+=", "
var a0=generateAccessor(a3[c],g,a2)
d+="'"+a0+"',"
var a1="p_"+a0
f+=a1
e+="this."+a0+" = "+a1+";\n"}if(supportsDirectProtoAccess)e+="this."+"$deferredAction"+"();"
f+=") {\n"+e+"}\n"
f+=a2+".builtin$cls=\""+a2+"\";\n"
f+="$desc=$collectedClasses."+a2+"[1];\n"
f+=a2+".prototype = $desc;\n"
if(typeof defineClass.name!="string")f+=a2+".name=\""+a2+"\";\n"
f+=a2+"."+"$__fields__"+"=["+d+"];\n"
f+=g.join("")
return f}init.createNewIsolate=function(){return new I()}
init.classIdExtractor=function(c){return c.constructor.name}
init.classFieldsExtractor=function(c){var g=c.constructor.$__fields__
if(!g)return[]
var f=[]
f.length=g.length
for(var e=0;e<g.length;e++)f[e]=c[g[e]]
return f}
init.instanceFromClassId=function(c){return new init.allClasses[c]()}
init.initializeEmptyInstance=function(c,d,e){init.allClasses[c].apply(d,e)
return d}
var z=supportsDirectProtoAccess?function(c,d){var g=c.prototype
g.__proto__=d.prototype
g.constructor=c
g["$is"+c.name]=c
return convertToFastObject(g)}:function(){function tmp(){}return function(a0,a1){tmp.prototype=a1.prototype
var g=new tmp()
convertToSlowObject(g)
var f=a0.prototype
var e=Object.keys(f)
for(var d=0;d<e.length;d++){var c=e[d]
g[c]=f[c]}g["$is"+a0.name]=a0
g.constructor=a0
a0.prototype=g
return g}}()
function finishClasses(a4){var g=init.allClasses
a4.combinedConstructorFunction+="return [\n"+a4.constructorsList.join(",\n  ")+"\n]"
var f=new Function("$collectedClasses",a4.combinedConstructorFunction)(a4.collected)
a4.combinedConstructorFunction=null
for(var e=0;e<f.length;e++){var d=f[e]
var c=d.name
var a0=a4.collected[c]
var a1=a0[0]
a0=a0[1]
g[c]=d
a1[c]=d}f=null
var a2=init.finishedClasses
function finishClass(c1){if(a2[c1])return
a2[c1]=true
var a5=a4.pending[c1]
if(a5&&a5.indexOf("+")>0){var a6=a5.split("+")
a5=a6[0]
var a7=a6[1]
finishClass(a7)
var a8=g[a7]
var a9=a8.prototype
var b0=g[c1].prototype
var b1=Object.keys(a9)
for(var b2=0;b2<b1.length;b2++){var b3=b1[b2]
if(!u.call(b0,b3))b0[b3]=a9[b3]}}if(!a5||typeof a5!="string"){var b4=g[c1]
var b5=b4.prototype
b5.constructor=b4
b5.$isa=b4
b5.$deferredAction=function(){}
return}finishClass(a5)
var b6=g[a5]
if(!b6)b6=existingIsolateProperties[a5]
var b4=g[c1]
var b5=z(b4,b6)
if(a9)b5.$deferredAction=mixinDeferredActionHelper(a9,b5)
if(Object.prototype.hasOwnProperty.call(b5,"%")){var b7=b5["%"].split(";")
if(b7[0]){var b8=b7[0].split("|")
for(var b2=0;b2<b8.length;b2++){init.interceptorsByTag[b8[b2]]=b4
init.leafTags[b8[b2]]=true}}if(b7[1]){b8=b7[1].split("|")
if(b7[2]){var b9=b7[2].split("|")
for(var b2=0;b2<b9.length;b2++){var c0=g[b9[b2]]
c0.$nativeSuperclassTag=b8[0]}}for(b2=0;b2<b8.length;b2++){init.interceptorsByTag[b8[b2]]=b4
init.leafTags[b8[b2]]=false}}b5.$deferredAction()}if(b5.$isf)b5.$deferredAction()}var a3=Object.keys(a4.pending)
for(var e=0;e<a3.length;e++)finishClass(a3[e])}function finishAddStubsHelper(){var g=this
while(!g.hasOwnProperty("$deferredAction"))g=g.__proto__
delete g.$deferredAction
var f=Object.keys(g)
for(var e=0;e<f.length;e++){var d=f[e]
var c=d.charCodeAt(0)
var a0
if(d!=="^"&&d!=="$reflectable"&&c!==43&&c!==42&&(a0=g[d])!=null&&a0.constructor===Array&&d!=="<>")addStubs(g,a0,d,false,[])}convertToFastObject(g)
g=g.__proto__
g.$deferredAction()}function mixinDeferredActionHelper(c,d){var g
if(d.hasOwnProperty("$deferredAction"))g=d.$deferredAction
return function foo(){var f=this
while(!f.hasOwnProperty("$deferredAction"))f=f.__proto__
if(g)f.$deferredAction=g
else{delete f.$deferredAction
convertToFastObject(f)}c.$deferredAction()
f.$deferredAction()}}function processClassData(b1,b2,b3){b2=convertToSlowObject(b2)
var g
var f=Object.keys(b2)
var e=false
var d=supportsDirectProtoAccess&&b1!="a"
for(var c=0;c<f.length;c++){var a0=f[c]
var a1=a0.charCodeAt(0)
if(a0==="n"){processStatics(init.statics[b1]=b2.n,b3)
delete b2.n}else if(a1===43){w[g]=a0.substring(1)
var a2=b2[a0]
if(a2>0)b2[g].$reflectable=a2}else if(a1===42){b2[g].$defaultValues=b2[a0]
var a3=b2.$methodsWithOptionalArguments
if(!a3)b2.$methodsWithOptionalArguments=a3={}
a3[a0]=g}else{var a4=b2[a0]
if(a0!=="^"&&a4!=null&&a4.constructor===Array&&a0!=="<>")if(d)e=true
else addStubs(b2,a4,a0,false,[])
else g=a0}}if(e)b2.$deferredAction=finishAddStubsHelper
var a5=b2["^"],a6,a7,a8=a5
var a9=a8.split(";")
a8=a9[1]?a9[1].split(","):[]
a7=a9[0]
a6=a7.split(":")
if(a6.length==2){a7=a6[0]
var b0=a6[1]
if(b0)b2.$signature=function(b4){return function(){return init.types[b4]}}(b0)}if(a7)b3.pending[b1]=a7
b3.combinedConstructorFunction+=defineClass(b1,a8)
b3.constructorsList.push(b1)
b3.collected[b1]=[m,b2]
i.push(b1)}function processStatics(a3,a4){var g=Object.keys(a3)
for(var f=0;f<g.length;f++){var e=g[f]
if(e==="^")continue
var d=a3[e]
var c=e.charCodeAt(0)
var a0
if(c===43){v[a0]=e.substring(1)
var a1=a3[e]
if(a1>0)a3[a0].$reflectable=a1
if(d&&d.length)init.typeInformation[a0]=d}else if(c===42){m[a0].$defaultValues=d
var a2=a3.$methodsWithOptionalArguments
if(!a2)a3.$methodsWithOptionalArguments=a2={}
a2[e]=a0}else if(typeof d==="function"){m[a0=e]=d
h.push(e)
init.globalFunctions[e]=d}else if(d.constructor===Array)addStubs(m,d,e,true,h)
else{a0=e
processClassData(e,d,a4)}}}function addStubs(b2,b3,b4,b5,b6){var g=0,f=b3[g],e
if(typeof f=="string")e=b3[++g]
else{e=f
f=b4}var d=[b2[b4]=b2[f]=e]
e.$stubName=b4
b6.push(b4)
for(g++;g<b3.length;g++){e=b3[g]
if(typeof e!="function")break
if(!b5)e.$stubName=b3[++g]
d.push(e)
if(e.$stubName){b2[e.$stubName]=e
b6.push(e.$stubName)}}for(var c=0;c<d.length;g++,c++)d[c].$callName=b3[g]
var a0=b3[g]
b3=b3.slice(++g)
var a1=b3[0]
var a2=a1>>1
var a3=(a1&1)===1
var a4=a1===3
var a5=a1===1
var a6=b3[1]
var a7=a6>>1
var a8=(a6&1)===1
var a9=a2+a7!=d[0].length
var b0=b3[2]
if(typeof b0=="number")b3[2]=b0+b
var b1=2*a7+a2+3
if(a0){e=tearOff(d,b3,b5,b4,a9)
b2[b4].$getter=e
e.$getterStub=true
if(b5){init.globalFunctions[b4]=e
b6.push(a0)}b2[a0]=e
d.push(e)
e.$stubName=a0
e.$callName=null}}function tearOffGetter(c,d,e,f){return f?new Function("funcs","reflectionInfo","name","H","c","return function tearOff_"+e+y+++"(x) {"+"if (c === null) c = "+"H.bu"+"("+"this, funcs, reflectionInfo, false, [x], name);"+"return new c(this, funcs[0], x, name);"+"}")(c,d,e,H,null):new Function("funcs","reflectionInfo","name","H","c","return function tearOff_"+e+y+++"() {"+"if (c === null) c = "+"H.bu"+"("+"this, funcs, reflectionInfo, false, [], name);"+"return new c(this, funcs[0], null, name);"+"}")(c,d,e,H,null)}function tearOff(c,d,e,f,a0){var g
return e?function(){if(g===void 0)g=H.bu(this,c,d,true,[],f).prototype
return g}:tearOffGetter(c,d,f,a0)}var y=0
if(!init.libraries)init.libraries=[]
if(!init.mangledNames)init.mangledNames=map()
if(!init.mangledGlobalNames)init.mangledGlobalNames=map()
if(!init.statics)init.statics=map()
if(!init.typeInformation)init.typeInformation=map()
if(!init.globalFunctions)init.globalFunctions=map()
var x=init.libraries
var w=init.mangledNames
var v=init.mangledGlobalNames
var u=Object.prototype.hasOwnProperty
var t=a.length
var s=map()
s.collected=map()
s.pending=map()
s.constructorsList=[]
s.combinedConstructorFunction="function $reflectable(fn){fn.$reflectable=1;return fn};\n"+"var $desc;\n"
for(var r=0;r<t;r++){var q=a[r]
var p=q[0]
var o=q[1]
var n=q[2]
var m=q[3]
var l=q[4]
var k=!!q[5]
var j=l&&l["^"]
if(j instanceof Array)j=j[0]
var i=[]
var h=[]
processStatics(l,s)
x.push([p,o,i,h,n,j,k,m])}finishClasses(s)}I.ak=function(){}
var dart=[["","",,H,{"^":"",hZ:{"^":"a;a"}}],["","",,J,{"^":"",
n:function(a){return void 0},
aW:function(a,b,c,d){return{i:a,p:b,e:c,x:d}},
aU:function(a){var z,y,x,w
z=a[init.dispatchPropertyName]
if(z==null)if($.bB==null){H.h9()
z=a[init.dispatchPropertyName]}if(z!=null){y=z.p
if(!1===y)return z.i
if(!0===y)return a
x=Object.getPrototypeOf(a)
if(y===x)return z.i
if(z.e===x)throw H.c(new P.cC("Return interceptor for "+H.b(y(a,z))))}w=H.hi(a)
if(w==null){if(typeof a=="function")return C.z
y=Object.getPrototypeOf(a)
if(y==null||y===Object.prototype)return C.B
else return C.C}return w},
f:{"^":"a;",
l:function(a,b){return a===b},
gt:function(a){return H.Q(a)},
i:["ck",function(a){return H.aF(a)}],
"%":"Blob|CanvasRenderingContext2D|DOMError|File|FileError|MediaError|MediaKeyError|NavigatorUserMediaError|PositionError|PushMessageData|SQLError|SVGAnimatedLength|SVGAnimatedLengthList|SVGAnimatedNumber|SVGAnimatedNumberList|SVGAnimatedString|SVGAnimatedTransformList|SVGRect|WebGLBuffer|WebGLProgram|WebGLShader|WebGLTexture|WebGLUniformLocation"},
e9:{"^":"f;",
i:function(a){return String(a)},
gt:function(a){return a?519018:218159},
$isfW:1},
ea:{"^":"f;",
l:function(a,b){return null==b},
i:function(a){return"null"},
gt:function(a){return 0}},
b9:{"^":"f;",
gt:function(a){return 0},
i:["cl",function(a){return String(a)}],
$iseb:1},
eq:{"^":"b9;"},
aM:{"^":"b9;"},
ap:{"^":"b9;",
i:function(a){var z=a[$.$get$bS()]
return z==null?this.cl(a):J.U(z)},
$signature:function(){return{func:1,opt:[,,,,,,,,,,,,,,,,]}}},
an:{"^":"f;",
bw:function(a,b){if(!!a.immutable$list)throw H.c(new P.N(b))},
cZ:function(a,b){if(!!a.fixed$length)throw H.c(new P.N(b))},
w:function(a,b){var z,y
z=a.length
for(y=0;y<z;++y){b.$1(a[y])
if(a.length!==z)throw H.c(new P.y(a))}},
Y:function(a,b){return H.i(new H.bd(a,b),[null,null])},
dw:function(a,b){var z,y,x,w
z=a.length
y=new Array(z)
y.fixed$length=Array
for(x=0;x<a.length;++x){w=H.b(a[x])
if(x>=z)return H.e(y,x)
y[x]=w}return y.join(b)},
J:function(a,b){if(b<0||b>=a.length)return H.e(a,b)
return a[b]},
gdh:function(a){if(a.length>0)return a[0]
throw H.c(H.c4())},
aZ:function(a,b,c,d,e){var z,y,x
this.bw(a,"set range")
P.cm(b,c,a.length,null,null,null)
z=c-b
if(z===0)return
if(e+z>d.length)throw H.c(H.e7())
if(e<b)for(y=z-1;y>=0;--y){x=e+y
if(x>=d.length)return H.e(d,x)
a[b+y]=d[x]}else for(y=0;y<z;++y){x=e+y
if(x>=d.length)return H.e(d,x)
a[b+y]=d[x]}},
i:function(a){return P.aB(a,"[","]")},
gA:function(a){return new J.dL(a,a.length,0,null)},
gt:function(a){return H.Q(a)},
gj:function(a){return a.length},
sj:function(a,b){this.cZ(a,"set length")
if(b<0)throw H.c(P.aG(b,0,null,"newLength",null))
a.length=b},
h:function(a,b){if(typeof b!=="number"||Math.floor(b)!==b)throw H.c(H.u(a,b))
if(b>=a.length||b<0)throw H.c(H.u(a,b))
return a[b]},
u:function(a,b,c){this.bw(a,"indexed set")
if(typeof b!=="number"||Math.floor(b)!==b)throw H.c(H.u(a,b))
if(b>=a.length||b<0)throw H.c(H.u(a,b))
a[b]=c},
$isac:1,
$asac:I.ak,
$isk:1,
$ask:null,
$ist:1},
hY:{"^":"an;"},
dL:{"^":"a;a,b,c,d",
gq:function(){return this.d},
p:function(){var z,y,x
z=this.a
y=z.length
if(this.b!==y)throw H.c(H.db(z))
x=this.c
if(x>=y){this.d=null
return!1}this.d=z[x]
this.c=x+1
return!0}},
ao:{"^":"f;",
aO:function(a,b){return a%b},
bM:function(a){if(a>0){if(a!==1/0)return Math.round(a)}else if(a>-1/0)return 0-Math.round(0-a)
throw H.c(new P.N(""+a+".round()"))},
i:function(a){if(a===0&&1/a<0)return"-0.0"
else return""+a},
gt:function(a){return a&0x1FFFFFFF},
k:function(a,b){if(typeof b!=="number")throw H.c(H.I(b))
return a+b},
Z:function(a,b){if(typeof b!=="number")throw H.c(H.I(b))
return a-b},
ac:function(a,b){if(typeof b!=="number")throw H.c(H.I(b))
return a/b},
L:function(a,b){if(typeof b!=="number")throw H.c(H.I(b))
return a*b},
a3:function(a,b){return(a|0)===a?a/b|0:this.cQ(a,b)},
cQ:function(a,b){var z=a/b
if(z>=-2147483648&&z<=2147483647)return z|0
if(z>0){if(z!==1/0)return Math.floor(z)}else if(z>-1/0)return Math.ceil(z)
throw H.c(new P.N("Result of truncating division is "+H.b(z)+": "+H.b(a)+" ~/ "+b))},
bo:function(a,b){var z
if(a>0)z=b>31?0:a>>>b
else{z=b>31?31:b
z=a>>z>>>0}return z},
al:function(a,b){if(typeof b!=="number")throw H.c(H.I(b))
return a<b},
$isa6:1},
c6:{"^":"ao;",$isa6:1,$isp:1},
c5:{"^":"ao;",$isa6:1},
aC:{"^":"f;",
k:function(a,b){if(typeof b!=="string")throw H.c(P.bN(b,null,null))
return a+b},
cj:function(a,b,c){H.cX(b)
if(c==null)c=a.length
H.cX(c)
if(b<0)throw H.c(P.aH(b,null,null))
if(typeof c!=="number")return H.x(c)
if(b>c)throw H.c(P.aH(b,null,null))
if(c>a.length)throw H.c(P.aH(c,null,null))
return a.substring(b,c)},
ci:function(a,b){return this.cj(a,b,null)},
L:function(a,b){var z,y
if(0>=b)return""
if(b===1||a.length===0)return a
if(b!==b>>>0)throw H.c(C.n)
for(z=a,y="";!0;){if((b&1)===1)y=z+y
b=b>>>1
if(b===0)break
z+=z}return y},
i:function(a){return a},
gt:function(a){var z,y,x
for(z=a.length,y=0,x=0;x<z;++x){y=536870911&y+a.charCodeAt(x)
y=536870911&y+((524287&y)<<10>>>0)
y^=y>>6}y=536870911&y+((67108863&y)<<3>>>0)
y^=y>>11
return 536870911&y+((16383&y)<<15>>>0)},
gj:function(a){return a.length},
h:function(a,b){if(typeof b!=="number"||Math.floor(b)!==b)throw H.c(H.u(a,b))
if(b>=a.length||b<0)throw H.c(H.u(a,b))
return a[b]},
$isac:1,
$asac:I.ak,
$isM:1}}],["","",,H,{"^":"",
c4:function(){return new P.aK("No element")},
e7:function(){return new P.aK("Too few elements")},
aq:{"^":"H;",
gA:function(a){return new H.c7(this,this.gj(this),0,null)},
w:function(a,b){var z,y
z=this.gj(this)
for(y=0;y<z;++y){b.$1(this.J(0,y))
if(z!==this.gj(this))throw H.c(new P.y(this))}},
Y:function(a,b){return H.i(new H.bd(this,b),[H.A(this,"aq",0),null])},
aT:function(a,b){var z,y,x
z=H.i([],[H.A(this,"aq",0)])
C.d.sj(z,this.gj(this))
for(y=0;y<this.gj(this);++y){x=this.J(0,y)
if(y>=z.length)return H.e(z,y)
z[y]=x}return z},
aS:function(a){return this.aT(a,!0)},
$ist:1},
c7:{"^":"a;a,b,c,d",
gq:function(){return this.d},
p:function(){var z,y,x,w
z=this.a
y=J.L(z)
x=y.gj(z)
if(this.b!==x)throw H.c(new P.y(z))
w=this.c
if(w>=x){this.d=null
return!1}this.d=y.J(z,w);++this.c
return!0}},
c9:{"^":"H;a,b",
gA:function(a){var z=new H.ej(null,J.b0(this.a),this.b)
z.$builtinTypeInfo=this.$builtinTypeInfo
return z},
gj:function(a){return J.al(this.a)},
$asH:function(a,b){return[b]},
n:{
aE:function(a,b,c,d){if(!!J.n(a).$ist)return H.i(new H.bU(a,b),[c,d])
return H.i(new H.c9(a,b),[c,d])}}},
bU:{"^":"c9;a,b",$ist:1},
ej:{"^":"e8;a,b,c",
p:function(){var z=this.b
if(z.p()){this.a=this.c.$1(z.gq())
return!0}this.a=null
return!1},
gq:function(){return this.a}},
bd:{"^":"aq;a,b",
gj:function(a){return J.al(this.a)},
J:function(a,b){return this.b.$1(J.dt(this.a,b))},
$asaq:function(a,b){return[b]},
$asH:function(a,b){return[b]},
$ist:1},
c_:{"^":"a;"}}],["","",,H,{"^":"",
as:function(a,b){var z=a.a6(b)
if(!init.globalState.d.cy)init.globalState.f.aa()
return z},
d9:function(a,b){var z,y,x,w,v,u
z={}
z.a=b
if(b==null){b=[]
z.a=b
y=b}else y=b
if(!J.n(y).$isk)throw H.c(P.b4("Arguments to main must be a List: "+H.b(y)))
init.globalState=new H.ft(0,0,1,null,null,null,null,null,null,null,null,null,a)
y=init.globalState
x=self.window==null
w=self.Worker
v=x&&!!self.postMessage
y.x=v
v=!v
if(v)w=w!=null&&$.$get$c2()!=null
else w=!0
y.y=w
y.r=x&&v
y.f=new H.f8(P.bb(null,H.ar),0)
y.z=H.i(new H.Z(0,null,null,null,null,null,0),[P.p,H.bo])
y.ch=H.i(new H.Z(0,null,null,null,null,null,0),[P.p,null])
if(y.x===!0){x=new H.fs()
y.Q=x
self.onmessage=function(c,d){return function(e){c(d,e)}}(H.e0,x)
self.dartPrint=self.dartPrint||function(c){return function(d){if(self.console&&self.console.log)self.console.log(d)
else self.postMessage(c(d))}}(H.fu)}if(init.globalState.x===!0)return
y=init.globalState.a++
x=H.i(new H.Z(0,null,null,null,null,null,0),[P.p,H.aI])
w=P.ad(null,null,null,P.p)
v=new H.aI(0,null,!1)
u=new H.bo(y,x,w,init.createNewIsolate(),v,new H.W(H.aY()),new H.W(H.aY()),!1,!1,[],P.ad(null,null,null,null),null,null,!1,!0,P.ad(null,null,null,null))
w.v(0,0)
u.b2(0,v)
init.globalState.e=u
init.globalState.d=u
y=H.av()
x=H.a4(y,[y]).O(a)
if(x)u.a6(new H.hp(z,a))
else{y=H.a4(y,[y,y]).O(a)
if(y)u.a6(new H.hq(z,a))
else u.a6(a)}init.globalState.f.aa()},
e4:function(){var z=init.currentScript
if(z!=null)return String(z.src)
if(init.globalState.x===!0)return H.e5()
return},
e5:function(){var z,y
z=new Error().stack
if(z==null){z=function(){try{throw new Error()}catch(x){return x.stack}}()
if(z==null)throw H.c(new P.N("No stack trace"))}y=z.match(new RegExp("^ *at [^(]*\\((.*):[0-9]*:[0-9]*\\)$","m"))
if(y!=null)return y[1]
y=z.match(new RegExp("^[^@]*@(.*):[0-9]*$","m"))
if(y!=null)return y[1]
throw H.c(new P.N('Cannot extract URI from "'+H.b(z)+'"'))},
e0:function(a,b){var z,y,x,w,v,u,t,s,r,q,p,o,n
z=new H.aN(!0,[]).P(b.data)
y=J.L(z)
switch(y.h(z,"command")){case"start":init.globalState.b=y.h(z,"id")
x=y.h(z,"functionName")
w=x==null?init.globalState.cx:init.globalFunctions[x]()
v=y.h(z,"args")
u=new H.aN(!0,[]).P(y.h(z,"msg"))
t=y.h(z,"isSpawnUri")
s=y.h(z,"startPaused")
r=new H.aN(!0,[]).P(y.h(z,"replyTo"))
y=init.globalState.a++
q=H.i(new H.Z(0,null,null,null,null,null,0),[P.p,H.aI])
p=P.ad(null,null,null,P.p)
o=new H.aI(0,null,!1)
n=new H.bo(y,q,p,init.createNewIsolate(),o,new H.W(H.aY()),new H.W(H.aY()),!1,!1,[],P.ad(null,null,null,null),null,null,!1,!0,P.ad(null,null,null,null))
p.v(0,0)
n.b2(0,o)
init.globalState.f.a.I(new H.ar(n,new H.e1(w,v,u,t,s,r),"worker-start"))
init.globalState.d=n
init.globalState.f.aa()
break
case"spawn-worker":break
case"message":if(y.h(z,"port")!=null)y.h(z,"port").M(y.h(z,"msg"))
init.globalState.f.aa()
break
case"close":init.globalState.ch.a9(0,$.$get$c3().h(0,a))
a.terminate()
init.globalState.f.aa()
break
case"log":H.e_(y.h(z,"msg"))
break
case"print":if(init.globalState.x===!0){y=init.globalState.Q
q=P.a_(["command","print","msg",z])
q=new H.a1(!0,P.ag(null,P.p)).B(q)
y.toString
self.postMessage(q)}else P.bD(y.h(z,"msg"))
break
case"error":throw H.c(y.h(z,"msg"))}},
e_:function(a){var z,y,x,w
if(init.globalState.x===!0){y=init.globalState.Q
x=P.a_(["command","log","msg",a])
x=new H.a1(!0,P.ag(null,P.p)).B(x)
y.toString
self.postMessage(x)}else try{self.console.log(a)}catch(w){H.G(w)
z=H.B(w)
throw H.c(P.E(z))}},
e2:function(a,b,c,d,e,f){var z,y,x,w
z=init.globalState.d
y=z.a
$.ci=$.ci+("_"+y)
$.cj=$.cj+("_"+y)
y=z.e
x=init.globalState.d.a
w=z.f
f.M(["spawned",new H.aP(y,x),w,z.r])
x=new H.e3(a,b,c,d,z)
if(e===!0){z.bt(w,w)
init.globalState.f.a.I(new H.ar(z,x,"start isolate"))}else x.$0()},
fL:function(a){return new H.aN(!0,[]).P(new H.a1(!1,P.ag(null,P.p)).B(a))},
hp:{"^":"d:0;a,b",
$0:function(){this.b.$1(this.a.a)}},
hq:{"^":"d:0;a,b",
$0:function(){this.b.$2(this.a.a,null)}},
ft:{"^":"a;a,b,c,d,e,f,r,x,y,z,Q,ch,cx",n:{
fu:function(a){var z=P.a_(["command","print","msg",a])
return new H.a1(!0,P.ag(null,P.p)).B(z)}}},
bo:{"^":"a;a,b,c,dv:d<,d3:e<,f,r,x,y,z,Q,ch,cx,cy,db,dx",
bt:function(a,b){if(!this.f.l(0,a))return
if(this.Q.v(0,b)&&!this.y)this.y=!0
this.aH()},
dE:function(a){var z,y,x,w,v,u
if(!this.y)return
z=this.Q
z.a9(0,a)
if(z.a===0){for(z=this.z;y=z.length,y!==0;){if(0>=y)return H.e(z,-1)
x=z.pop()
y=init.globalState.f.a
w=y.b
v=y.a
u=v.length
w=(w-1&u-1)>>>0
y.b=w
if(w<0||w>=u)return H.e(v,w)
v[w]=x
if(w===y.c)y.b8();++y.d}this.y=!1}this.aH()},
cT:function(a,b){var z,y,x
if(this.ch==null)this.ch=[]
for(z=J.n(a),y=0;x=this.ch,y<x.length;y+=2)if(z.l(a,x[y])){z=this.ch
x=y+1
if(x>=z.length)return H.e(z,x)
z[x]=b
return}x.push(a)
this.ch.push(b)},
dD:function(a){var z,y,x
if(this.ch==null)return
for(z=J.n(a),y=0;x=this.ch,y<x.length;y+=2)if(z.l(a,x[y])){z=this.ch
x=y+2
z.toString
if(typeof z!=="object"||z===null||!!z.fixed$length)H.r(new P.N("removeRange"))
P.cm(y,x,z.length,null,null,null)
z.splice(y,x-y)
return}},
ce:function(a,b){if(!this.r.l(0,a))return
this.db=b},
dl:function(a,b,c){var z=J.n(b)
if(!z.l(b,0))z=z.l(b,1)&&!this.cy
else z=!0
if(z){a.M(c)
return}z=this.cx
if(z==null){z=P.bb(null,null)
this.cx=z}z.I(new H.fn(a,c))},
dk:function(a,b){var z
if(!this.r.l(0,a))return
z=J.n(b)
if(!z.l(b,0))z=z.l(b,1)&&!this.cy
else z=!0
if(z){this.aK()
return}z=this.cx
if(z==null){z=P.bb(null,null)
this.cx=z}z.I(this.gdz())},
dm:function(a,b){var z,y,x
z=this.dx
if(z.a===0){if(this.db===!0&&this===init.globalState.e)return
if(self.console&&self.console.error)self.console.error(a,b)
else{P.bD(a)
if(b!=null)P.bD(b)}return}y=new Array(2)
y.fixed$length=Array
y[0]=J.U(a)
y[1]=b==null?null:J.U(b)
for(x=new P.bp(z,z.r,null,null),x.c=z.e;x.p();)x.d.M(y)},
a6:function(a){var z,y,x,w,v,u,t
z=init.globalState.d
init.globalState.d=this
$=this.d
y=null
x=this.cy
this.cy=!0
try{y=a.$0()}catch(u){t=H.G(u)
w=t
v=H.B(u)
this.dm(w,v)
if(this.db===!0){this.aK()
if(this===init.globalState.e)throw u}}finally{this.cy=x
init.globalState.d=z
if(z!=null)$=z.gdv()
if(this.cx!=null)for(;t=this.cx,!t.gK(t);)this.cx.bK().$0()}return y},
bH:function(a){return this.b.h(0,a)},
b2:function(a,b){var z=this.b
if(z.bx(a))throw H.c(P.E("Registry: ports must be registered only once."))
z.u(0,a,b)},
aH:function(){var z=this.b
if(z.gj(z)-this.c.a>0||this.y||!this.x)init.globalState.z.u(0,this.a,this)
else this.aK()},
aK:[function(){var z,y,x,w,v
z=this.cx
if(z!=null)z.W(0)
for(z=this.b,y=z.gbU(z),y=y.gA(y);y.p();)y.gq().cw()
z.W(0)
this.c.W(0)
init.globalState.z.a9(0,this.a)
this.dx.W(0)
if(this.ch!=null){for(x=0;z=this.ch,y=z.length,x<y;x+=2){w=z[x]
v=x+1
if(v>=y)return H.e(z,v)
w.M(z[v])}this.ch=null}},"$0","gdz",0,0,1]},
fn:{"^":"d:1;a,b",
$0:function(){this.a.M(this.b)}},
f8:{"^":"a;a,b",
d8:function(){var z=this.a
if(z.b===z.c)return
return z.bK()},
bP:function(){var z,y,x
z=this.d8()
if(z==null){if(init.globalState.e!=null)if(init.globalState.z.bx(init.globalState.e.a))if(init.globalState.r===!0){y=init.globalState.e.b
y=y.gK(y)}else y=!1
else y=!1
else y=!1
if(y)H.r(P.E("Program exited with open ReceivePorts."))
y=init.globalState
if(y.x===!0){x=y.z
x=x.gK(x)&&y.f.b===0}else x=!1
if(x){y=y.Q
x=P.a_(["command","close"])
x=new H.a1(!0,H.i(new P.cL(0,null,null,null,null,null,0),[null,P.p])).B(x)
y.toString
self.postMessage(x)}return!1}z.dC()
return!0},
bk:function(){if(self.window!=null)new H.f9(this).$0()
else for(;this.bP(););},
aa:function(){var z,y,x,w,v
if(init.globalState.x!==!0)this.bk()
else try{this.bk()}catch(x){w=H.G(x)
z=w
y=H.B(x)
w=init.globalState.Q
v=P.a_(["command","error","msg",H.b(z)+"\n"+H.b(y)])
v=new H.a1(!0,P.ag(null,P.p)).B(v)
w.toString
self.postMessage(v)}}},
f9:{"^":"d:1;a",
$0:function(){if(!this.a.bP())return
P.eS(C.h,this)}},
ar:{"^":"a;a,b,c",
dC:function(){var z=this.a
if(z.y){z.z.push(this)
return}z.a6(this.b)}},
fs:{"^":"a;"},
e1:{"^":"d:0;a,b,c,d,e,f",
$0:function(){H.e2(this.a,this.b,this.c,this.d,this.e,this.f)}},
e3:{"^":"d:1;a,b,c,d,e",
$0:function(){var z,y,x,w
z=this.e
z.x=!0
if(this.d!==!0)this.a.$1(this.c)
else{y=this.a
x=H.av()
w=H.a4(x,[x,x]).O(y)
if(w)y.$2(this.b,this.c)
else{x=H.a4(x,[x]).O(y)
if(x)y.$1(this.b)
else y.$0()}}z.aH()}},
cF:{"^":"a;"},
aP:{"^":"cF;b,a",
M:function(a){var z,y,x
z=init.globalState.z.h(0,this.a)
if(z==null)return
y=this.b
if(y.gbb())return
x=H.fL(a)
if(z.gd3()===y){y=J.L(x)
switch(y.h(x,0)){case"pause":z.bt(y.h(x,1),y.h(x,2))
break
case"resume":z.dE(y.h(x,1))
break
case"add-ondone":z.cT(y.h(x,1),y.h(x,2))
break
case"remove-ondone":z.dD(y.h(x,1))
break
case"set-errors-fatal":z.ce(y.h(x,1),y.h(x,2))
break
case"ping":z.dl(y.h(x,1),y.h(x,2),y.h(x,3))
break
case"kill":z.dk(y.h(x,1),y.h(x,2))
break
case"getErrors":y=y.h(x,1)
z.dx.v(0,y)
break
case"stopErrors":y=y.h(x,1)
z.dx.a9(0,y)
break}return}init.globalState.f.a.I(new H.ar(z,new H.fw(this,x),"receive"))},
l:function(a,b){if(b==null)return!1
return b instanceof H.aP&&J.T(this.b,b.b)},
gt:function(a){return this.b.gaA()}},
fw:{"^":"d:0;a,b",
$0:function(){var z=this.a.b
if(!z.gbb())z.ct(this.b)}},
br:{"^":"cF;b,c,a",
M:function(a){var z,y,x
z=P.a_(["command","message","port",this,"msg",a])
y=new H.a1(!0,P.ag(null,P.p)).B(z)
if(init.globalState.x===!0){init.globalState.Q.toString
self.postMessage(y)}else{x=init.globalState.ch.h(0,this.b)
if(x!=null)x.postMessage(y)}},
l:function(a,b){if(b==null)return!1
return b instanceof H.br&&J.T(this.b,b.b)&&J.T(this.a,b.a)&&J.T(this.c,b.c)},
gt:function(a){var z,y,x
z=this.b
if(typeof z!=="number")return z.cg()
y=this.a
if(typeof y!=="number")return y.cg()
x=this.c
if(typeof x!=="number")return H.x(x)
return(z<<16^y<<8^x)>>>0}},
aI:{"^":"a;aA:a<,b,bb:c<",
cw:function(){this.c=!0
this.b=null},
ct:function(a){if(this.c)return
this.b.$1(a)},
$iset:1},
eO:{"^":"a;a,b,c",
cq:function(a,b){var z,y
if(a===0)z=self.setTimeout==null||init.globalState.x===!0
else z=!1
if(z){this.c=1
z=init.globalState.f
y=init.globalState.d
z.a.I(new H.ar(y,new H.eQ(this,b),"timer"))
this.b=!0}else if(self.setTimeout!=null){++init.globalState.f.b
this.c=self.setTimeout(H.a5(new H.eR(this,b),0),a)}else throw H.c(new P.N("Timer greater than 0."))},
n:{
eP:function(a,b){var z=new H.eO(!0,!1,null)
z.cq(a,b)
return z}}},
eQ:{"^":"d:1;a,b",
$0:function(){this.a.c=null
this.b.$0()}},
eR:{"^":"d:1;a,b",
$0:function(){this.a.c=null;--init.globalState.f.b
this.b.$0()}},
W:{"^":"a;aA:a<",
gt:function(a){var z=this.a
if(typeof z!=="number")return z.dT()
z=C.a.bo(z,0)^C.a.a3(z,4294967296)
z=(~z>>>0)+(z<<15>>>0)&4294967295
z=((z^z>>>12)>>>0)*5&4294967295
z=((z^z>>>4)>>>0)*2057&4294967295
return(z^z>>>16)>>>0},
l:function(a,b){var z,y
if(b==null)return!1
if(b===this)return!0
if(b instanceof H.W){z=this.a
y=b.a
return z==null?y==null:z===y}return!1}},
a1:{"^":"a;a,b",
B:[function(a){var z,y,x,w,v
if(a==null||typeof a==="string"||typeof a==="number"||typeof a==="boolean")return a
z=this.b
y=z.h(0,a)
if(y!=null)return["ref",y]
z.u(0,a,z.gj(z))
z=J.n(a)
if(!!z.$isca)return["buffer",a]
if(!!z.$isbg)return["typed",a]
if(!!z.$isac)return this.ca(a)
if(!!z.$isdZ){x=this.gc7()
w=a.gbE()
w=H.aE(w,x,H.A(w,"H",0),null)
w=P.bc(w,!0,H.A(w,"H",0))
z=z.gbU(a)
z=H.aE(z,x,H.A(z,"H",0),null)
return["map",w,P.bc(z,!0,H.A(z,"H",0))]}if(!!z.$iseb)return this.cb(a)
if(!!z.$isf)this.bT(a)
if(!!z.$iset)this.ab(a,"RawReceivePorts can't be transmitted:")
if(!!z.$isaP)return this.cc(a)
if(!!z.$isbr)return this.cd(a)
if(!!z.$isd){v=a.$static_name
if(v==null)this.ab(a,"Closures can't be transmitted:")
return["function",v]}if(!!z.$isW)return["capability",a.a]
if(!(a instanceof P.a))this.bT(a)
return["dart",init.classIdExtractor(a),this.c9(init.classFieldsExtractor(a))]},"$1","gc7",2,0,2],
ab:function(a,b){throw H.c(new P.N(H.b(b==null?"Can't transmit:":b)+" "+H.b(a)))},
bT:function(a){return this.ab(a,null)},
ca:function(a){var z=this.c8(a)
if(!!a.fixed$length)return["fixed",z]
if(!a.fixed$length)return["extendable",z]
if(!a.immutable$list)return["mutable",z]
if(a.constructor===Array)return["const",z]
this.ab(a,"Can't serialize indexable: ")},
c8:function(a){var z,y,x
z=[]
C.d.sj(z,a.length)
for(y=0;y<a.length;++y){x=this.B(a[y])
if(y>=z.length)return H.e(z,y)
z[y]=x}return z},
c9:function(a){var z
for(z=0;z<a.length;++z)C.d.u(a,z,this.B(a[z]))
return a},
cb:function(a){var z,y,x,w
if(!!a.constructor&&a.constructor!==Object)this.ab(a,"Only plain JS Objects are supported:")
z=Object.keys(a)
y=[]
C.d.sj(y,z.length)
for(x=0;x<z.length;++x){w=this.B(a[z[x]])
if(x>=y.length)return H.e(y,x)
y[x]=w}return["js-object",z,y]},
cd:function(a){if(this.a)return["sendport",a.b,a.a,a.c]
return["raw sendport",a]},
cc:function(a){if(this.a)return["sendport",init.globalState.b,a.a,a.b.gaA()]
return["raw sendport",a]}},
aN:{"^":"a;a,b",
P:[function(a){var z,y,x,w,v,u
if(a==null||typeof a==="string"||typeof a==="number"||typeof a==="boolean")return a
if(typeof a!=="object"||a===null||a.constructor!==Array)throw H.c(P.b4("Bad serialized message: "+H.b(a)))
switch(C.d.gdh(a)){case"ref":if(1>=a.length)return H.e(a,1)
z=a[1]
y=this.b
if(z>>>0!==z||z>=y.length)return H.e(y,z)
return y[z]
case"buffer":if(1>=a.length)return H.e(a,1)
x=a[1]
this.b.push(x)
return x
case"typed":if(1>=a.length)return H.e(a,1)
x=a[1]
this.b.push(x)
return x
case"fixed":if(1>=a.length)return H.e(a,1)
x=a[1]
this.b.push(x)
y=H.i(this.a5(x),[null])
y.fixed$length=Array
return y
case"extendable":if(1>=a.length)return H.e(a,1)
x=a[1]
this.b.push(x)
return H.i(this.a5(x),[null])
case"mutable":if(1>=a.length)return H.e(a,1)
x=a[1]
this.b.push(x)
return this.a5(x)
case"const":if(1>=a.length)return H.e(a,1)
x=a[1]
this.b.push(x)
y=H.i(this.a5(x),[null])
y.fixed$length=Array
return y
case"map":return this.dc(a)
case"sendport":return this.dd(a)
case"raw sendport":if(1>=a.length)return H.e(a,1)
x=a[1]
this.b.push(x)
return x
case"js-object":return this.da(a)
case"function":if(1>=a.length)return H.e(a,1)
x=init.globalFunctions[a[1]]()
this.b.push(x)
return x
case"capability":if(1>=a.length)return H.e(a,1)
return new H.W(a[1])
case"dart":y=a.length
if(1>=y)return H.e(a,1)
w=a[1]
if(2>=y)return H.e(a,2)
v=a[2]
u=init.instanceFromClassId(w)
this.b.push(u)
this.a5(v)
return init.initializeEmptyInstance(w,u,v)
default:throw H.c("couldn't deserialize: "+H.b(a))}},"$1","gd9",2,0,2],
a5:function(a){var z,y,x
z=J.L(a)
y=0
while(!0){x=z.gj(a)
if(typeof x!=="number")return H.x(x)
if(!(y<x))break
z.u(a,y,this.P(z.h(a,y)));++y}return a},
dc:function(a){var z,y,x,w,v,u
z=a.length
if(1>=z)return H.e(a,1)
y=a[1]
if(2>=z)return H.e(a,2)
x=a[2]
w=P.eh()
this.b.push(w)
y=J.dB(y,this.gd9()).aS(0)
for(z=J.L(y),v=J.L(x),u=0;u<z.gj(y);++u){if(u>=y.length)return H.e(y,u)
w.u(0,y[u],this.P(v.h(x,u)))}return w},
dd:function(a){var z,y,x,w,v,u,t
z=a.length
if(1>=z)return H.e(a,1)
y=a[1]
if(2>=z)return H.e(a,2)
x=a[2]
if(3>=z)return H.e(a,3)
w=a[3]
if(J.T(y,init.globalState.b)){v=init.globalState.z.h(0,x)
if(v==null)return
u=v.bH(w)
if(u==null)return
t=new H.aP(u,x)}else t=new H.br(y,w,x)
this.b.push(t)
return t},
da:function(a){var z,y,x,w,v,u,t
z=a.length
if(1>=z)return H.e(a,1)
y=a[1]
if(2>=z)return H.e(a,2)
x=a[2]
w={}
this.b.push(w)
z=J.L(y)
v=J.L(x)
u=0
while(!0){t=z.gj(y)
if(typeof t!=="number")return H.x(t)
if(!(u<t))break
w[z.h(y,u)]=this.P(v.h(x,u));++u}return w}}}],["","",,H,{"^":"",
d1:function(a){return init.getTypeFromName(a)},
h0:function(a){return init.types[a]},
hh:function(a,b){var z
if(b!=null){z=b.x
if(z!=null)return z}return!!J.n(a).$isaD},
b:function(a){var z
if(typeof a==="string")return a
if(typeof a==="number"){if(a!==0)return""+a}else if(!0===a)return"true"
else if(!1===a)return"false"
else if(a==null)return"null"
z=J.U(a)
if(typeof z!=="string")throw H.c(H.I(a))
return z},
Q:function(a){var z=a.$identityHash
if(z==null){z=Math.random()*0x3fffffff|0
a.$identityHash=z}return z},
ck:function(a){var z,y,x,w,v,u,t,s,r
z=J.n(a)
y=z.constructor
if(typeof y=="function"){x=y.name
w=typeof x==="string"?x:null}else w=null
if(w==null||z===C.q||!!J.n(a).$isaM){v=C.k(a)
if(v==="Object"){u=a.constructor
if(typeof u=="function"){t=String(u).match(/^\s*function\s*([\w$]*)\s*\(/)
s=t==null?null:t[1]
if(typeof s==="string"&&/^\w+$/.test(s))w=s}if(w==null)w=v}else w=v}w=w
if(w.length>1)r=w.charCodeAt(0)===36
else r=!1
if(r)w=C.i.ci(w,1)
return function(b,c){return b.replace(/[^<,> ]+/g,function(d){return c[d]||d})}(w+H.d0(H.by(a),0,null),init.mangledGlobalNames)},
aF:function(a){return"Instance of '"+H.ck(a)+"'"},
bh:function(a,b){if(a==null||typeof a==="boolean"||typeof a==="number"||typeof a==="string")throw H.c(H.I(a))
return a[b]},
cl:function(a,b,c){if(a==null||typeof a==="boolean"||typeof a==="number"||typeof a==="string")throw H.c(H.I(a))
a[b]=c},
x:function(a){throw H.c(H.I(a))},
e:function(a,b){if(a==null)J.al(a)
throw H.c(H.u(a,b))},
u:function(a,b){var z,y
if(typeof b!=="number"||Math.floor(b)!==b)return new P.V(!0,b,"index",null)
z=J.al(a)
if(!(b<0)){if(typeof z!=="number")return H.x(z)
y=b>=z}else y=!0
if(y)return P.b8(b,a,"index",null,z)
return P.aH(b,"index",null)},
I:function(a){return new P.V(!0,a,null,null)},
z:function(a){if(typeof a!=="number")throw H.c(H.I(a))
return a},
cX:function(a){if(typeof a!=="number"||Math.floor(a)!==a)throw H.c(H.I(a))
return a},
c:function(a){var z
if(a==null)a=new P.cg()
z=new Error()
z.dartException=a
if("defineProperty" in Object){Object.defineProperty(z,"message",{get:H.dd})
z.name=""}else z.toString=H.dd
return z},
dd:function(){return J.U(this.dartException)},
r:function(a){throw H.c(a)},
db:function(a){throw H.c(new P.y(a))},
G:function(a){var z,y,x,w,v,u,t,s,r,q,p,o,n,m,l
z=new H.hs(a)
if(a==null)return
if(typeof a!=="object")return a
if("dartException" in a)return z.$1(a.dartException)
else if(!("message" in a))return a
y=a.message
if("number" in a&&typeof a.number=="number"){x=a.number
w=x&65535
if((C.c.bo(x,16)&8191)===10)switch(w){case 438:return z.$1(H.ba(H.b(y)+" (Error "+w+")",null))
case 445:case 5007:v=H.b(y)+" (Error "+w+")"
return z.$1(new H.cf(v,null))}}if(a instanceof TypeError){u=$.$get$cr()
t=$.$get$cs()
s=$.$get$ct()
r=$.$get$cu()
q=$.$get$cy()
p=$.$get$cz()
o=$.$get$cw()
$.$get$cv()
n=$.$get$cB()
m=$.$get$cA()
l=u.C(y)
if(l!=null)return z.$1(H.ba(y,l))
else{l=t.C(y)
if(l!=null){l.method="call"
return z.$1(H.ba(y,l))}else{l=s.C(y)
if(l==null){l=r.C(y)
if(l==null){l=q.C(y)
if(l==null){l=p.C(y)
if(l==null){l=o.C(y)
if(l==null){l=r.C(y)
if(l==null){l=n.C(y)
if(l==null){l=m.C(y)
v=l!=null}else v=!0}else v=!0}else v=!0}else v=!0}else v=!0}else v=!0}else v=!0
if(v)return z.$1(new H.cf(y,l==null?null:l.method))}}return z.$1(new H.eU(typeof y==="string"?y:""))}if(a instanceof RangeError){if(typeof y==="string"&&y.indexOf("call stack")!==-1)return new P.co()
y=function(b){try{return String(b)}catch(k){}return null}(a)
return z.$1(new P.V(!1,null,null,typeof y==="string"?y.replace(/^RangeError:\s*/,""):y))}if(typeof InternalError=="function"&&a instanceof InternalError)if(typeof y==="string"&&y==="too much recursion")return new P.co()
return a},
B:function(a){var z
if(a==null)return new H.cM(a,null)
z=a.$cachedTrace
if(z!=null)return z
return a.$cachedTrace=new H.cM(a,null)},
hl:function(a){if(a==null||typeof a!='object')return J.ay(a)
else return H.Q(a)},
fZ:function(a,b){var z,y,x,w
z=a.length
for(y=0;y<z;y=w){x=y+1
w=x+1
b.u(0,a[y],a[x])}return b},
hb:function(a,b,c,d,e,f,g){switch(c){case 0:return H.as(b,new H.hc(a))
case 1:return H.as(b,new H.hd(a,d))
case 2:return H.as(b,new H.he(a,d,e))
case 3:return H.as(b,new H.hf(a,d,e,f))
case 4:return H.as(b,new H.hg(a,d,e,f,g))}throw H.c(P.E("Unsupported number of arguments for wrapped closure"))},
a5:function(a,b){var z
if(a==null)return
z=a.$identity
if(!!z)return z
z=function(c,d,e,f){return function(g,h,i,j){return f(c,e,d,g,h,i,j)}}(a,b,init.globalState.d,H.hb)
a.$identity=z
return z},
dS:function(a,b,c,d,e,f){var z,y,x,w,v,u,t,s,r,q,p,o,n,m
z=b[0]
y=z.$callName
if(!!J.n(c).$isk){z.$reflectionInfo=c
x=H.ev(z).r}else x=c
w=d?Object.create(new H.eD().constructor.prototype):Object.create(new H.b5(null,null,null,null).constructor.prototype)
w.$initialize=w.constructor
if(d)v=function(){this.$initialize()}
else{u=$.J
$.J=J.a7(u,1)
u=new Function("a,b,c,d"+u,"this.$initialize(a,b,c,d"+u+")")
v=u}w.constructor=v
v.prototype=w
u=!d
if(u){t=e.length==1&&!0
s=H.bR(a,z,t)
s.$reflectionInfo=c}else{w.$static_name=f
s=z
t=!1}if(typeof x=="number")r=function(g,h){return function(){return g(h)}}(H.h0,x)
else if(u&&typeof x=="function"){q=t?H.bP:H.b6
r=function(g,h){return function(){return g.apply({$receiver:h(this)},arguments)}}(x,q)}else throw H.c("Error in reflectionInfo.")
w.$signature=r
w[y]=s
for(u=b.length,p=1;p<u;++p){o=b[p]
n=o.$callName
if(n!=null){m=d?o:H.bR(a,o,t)
w[n]=m}}w["call*"]=s
w.$requiredArgCount=z.$requiredArgCount
w.$defaultValues=z.$defaultValues
return v},
dP:function(a,b,c,d){var z=H.b6
switch(b?-1:a){case 0:return function(e,f){return function(){return f(this)[e]()}}(c,z)
case 1:return function(e,f){return function(g){return f(this)[e](g)}}(c,z)
case 2:return function(e,f){return function(g,h){return f(this)[e](g,h)}}(c,z)
case 3:return function(e,f){return function(g,h,i){return f(this)[e](g,h,i)}}(c,z)
case 4:return function(e,f){return function(g,h,i,j){return f(this)[e](g,h,i,j)}}(c,z)
case 5:return function(e,f){return function(g,h,i,j,k){return f(this)[e](g,h,i,j,k)}}(c,z)
default:return function(e,f){return function(){return e.apply(f(this),arguments)}}(d,z)}},
bR:function(a,b,c){var z,y,x,w,v,u,t
if(c)return H.dR(a,b)
z=b.$stubName
y=b.length
x=a[z]
w=b==null?x==null:b===x
v=!w||y>=27
if(v)return H.dP(y,!w,z,b)
if(y===0){w=$.J
$.J=J.a7(w,1)
u="self"+H.b(w)
w="return function(){var "+u+" = this."
v=$.aa
if(v==null){v=H.aA("self")
$.aa=v}return new Function(w+H.b(v)+";return "+u+"."+H.b(z)+"();}")()}t="abcdefghijklmnopqrstuvwxyz".split("").splice(0,y).join(",")
w=$.J
$.J=J.a7(w,1)
t+=H.b(w)
w="return function("+t+"){return this."
v=$.aa
if(v==null){v=H.aA("self")
$.aa=v}return new Function(w+H.b(v)+"."+H.b(z)+"("+t+");}")()},
dQ:function(a,b,c,d){var z,y
z=H.b6
y=H.bP
switch(b?-1:a){case 0:throw H.c(new H.ew("Intercepted function with no arguments."))
case 1:return function(e,f,g){return function(){return f(this)[e](g(this))}}(c,z,y)
case 2:return function(e,f,g){return function(h){return f(this)[e](g(this),h)}}(c,z,y)
case 3:return function(e,f,g){return function(h,i){return f(this)[e](g(this),h,i)}}(c,z,y)
case 4:return function(e,f,g){return function(h,i,j){return f(this)[e](g(this),h,i,j)}}(c,z,y)
case 5:return function(e,f,g){return function(h,i,j,k){return f(this)[e](g(this),h,i,j,k)}}(c,z,y)
case 6:return function(e,f,g){return function(h,i,j,k,l){return f(this)[e](g(this),h,i,j,k,l)}}(c,z,y)
default:return function(e,f,g,h){return function(){h=[g(this)]
Array.prototype.push.apply(h,arguments)
return e.apply(f(this),h)}}(d,z,y)}},
dR:function(a,b){var z,y,x,w,v,u,t,s
z=H.dN()
y=$.bO
if(y==null){y=H.aA("receiver")
$.bO=y}x=b.$stubName
w=b.length
v=a[x]
u=b==null?v==null:b===v
t=!u||w>=28
if(t)return H.dQ(w,!u,x,b)
if(w===1){y="return function(){return this."+H.b(z)+"."+H.b(x)+"(this."+H.b(y)+");"
u=$.J
$.J=J.a7(u,1)
return new Function(y+H.b(u)+"}")()}s="abcdefghijklmnopqrstuvwxyz".split("").splice(0,w-1).join(",")
y="return function("+s+"){return this."+H.b(z)+"."+H.b(x)+"(this."+H.b(y)+", "+s+");"
u=$.J
$.J=J.a7(u,1)
return new Function(y+H.b(u)+"}")()},
bu:function(a,b,c,d,e,f){var z
b.fixed$length=Array
if(!!J.n(c).$isk){c.fixed$length=Array
z=c}else z=c
return H.dS(a,b,z,!!d,e,f)},
hr:function(a){throw H.c(new P.dT("Cyclic initialization for static "+H.b(a)))},
a4:function(a,b,c){return new H.ex(a,b,c,null)},
cW:function(a,b){var z=a.builtin$cls
if(b==null||b.length===0)return new H.ez(z)
return new H.ey(z,b,null)},
av:function(){return C.m},
aY:function(){return(Math.random()*0x100000000>>>0)+(Math.random()*0x100000000>>>0)*4294967296},
i:function(a,b){a.$builtinTypeInfo=b
return a},
by:function(a){if(a==null)return
return a.$builtinTypeInfo},
cZ:function(a,b){return H.da(a["$as"+H.b(b)],H.by(a))},
A:function(a,b,c){var z=H.cZ(a,b)
return z==null?null:z[c]},
F:function(a,b){var z=H.by(a)
return z==null?null:z[b]},
bE:function(a,b){if(a==null)return"dynamic"
else if(typeof a==="object"&&a!==null&&a.constructor===Array)return a[0].builtin$cls+H.d0(a,1,b)
else if(typeof a=="function")return a.builtin$cls
else if(typeof a==="number"&&Math.floor(a)===a)return C.c.i(a)
else return},
d0:function(a,b,c){var z,y,x,w,v,u
if(a==null)return""
z=new P.bj("")
for(y=b,x=!0,w=!0,v="";y<a.length;++y){if(x)x=!1
else z.a=v+", "
u=a[y]
if(u!=null)w=!1
v=z.a+=H.b(H.bE(u,c))}return w?"":"<"+H.b(z)+">"},
da:function(a,b){if(typeof a=="function"){a=a.apply(null,b)
if(a==null)return a
if(typeof a==="object"&&a!==null&&a.constructor===Array)return a
if(typeof a=="function")return a.apply(null,b)}return b},
fS:function(a,b){var z,y
if(a==null||b==null)return!0
z=a.length
for(y=0;y<z;++y)if(!H.C(a[y],b[y]))return!1
return!0},
bv:function(a,b,c){return a.apply(b,H.cZ(b,c))},
C:function(a,b){var z,y,x,w,v
if(a===b)return!0
if(a==null||b==null)return!0
if('func' in b)return H.d_(a,b)
if('func' in a)return b.builtin$cls==="hU"
z=typeof a==="object"&&a!==null&&a.constructor===Array
y=z?a[0]:a
x=typeof b==="object"&&b!==null&&b.constructor===Array
w=x?b[0]:b
if(w!==y){if(!('$is'+H.bE(w,null) in y.prototype))return!1
v=y.prototype["$as"+H.b(H.bE(w,null))]}else v=null
if(!z&&v==null||!x)return!0
z=z?a.slice(1):null
x=x?b.slice(1):null
return H.fS(H.da(v,z),x)},
cU:function(a,b,c){var z,y,x,w,v
z=b==null
if(z&&a==null)return!0
if(z)return c
if(a==null)return!1
y=a.length
x=b.length
if(c){if(y<x)return!1}else if(y!==x)return!1
for(w=0;w<x;++w){z=a[w]
v=b[w]
if(!(H.C(z,v)||H.C(v,z)))return!1}return!0},
fR:function(a,b){var z,y,x,w,v,u
if(b==null)return!0
if(a==null)return!1
z=Object.getOwnPropertyNames(b)
z.fixed$length=Array
y=z
for(z=y.length,x=0;x<z;++x){w=y[x]
if(!Object.hasOwnProperty.call(a,w))return!1
v=b[w]
u=a[w]
if(!(H.C(v,u)||H.C(u,v)))return!1}return!0},
d_:function(a,b){var z,y,x,w,v,u,t,s,r,q,p,o,n,m,l
if(!('func' in a))return!1
if("v" in a){if(!("v" in b)&&"ret" in b)return!1}else if(!("v" in b)){z=a.ret
y=b.ret
if(!(H.C(z,y)||H.C(y,z)))return!1}x=a.args
w=b.args
v=a.opt
u=b.opt
t=x!=null?x.length:0
s=w!=null?w.length:0
r=v!=null?v.length:0
q=u!=null?u.length:0
if(t>s)return!1
if(t+r<s+q)return!1
if(t===s){if(!H.cU(x,w,!1))return!1
if(!H.cU(v,u,!0))return!1}else{for(p=0;p<t;++p){o=x[p]
n=w[p]
if(!(H.C(o,n)||H.C(n,o)))return!1}for(m=p,l=0;m<s;++l,++m){o=v[l]
n=w[m]
if(!(H.C(o,n)||H.C(n,o)))return!1}for(m=0;m<q;++l,++m){o=v[l]
n=u[m]
if(!(H.C(o,n)||H.C(n,o)))return!1}}return H.fR(a.named,b.named)},
iI:function(a){var z=$.bz
return"Instance of "+(z==null?"<Unknown>":z.$1(a))},
iE:function(a){return H.Q(a)},
iD:function(a,b,c){Object.defineProperty(a,b,{value:c,enumerable:false,writable:true,configurable:true})},
hi:function(a){var z,y,x,w,v,u
z=$.bz.$1(a)
y=$.aT[z]
if(y!=null){Object.defineProperty(a,init.dispatchPropertyName,{value:y,enumerable:false,writable:true,configurable:true})
return y.i}x=$.aV[z]
if(x!=null)return x
w=init.interceptorsByTag[z]
if(w==null){z=$.cT.$2(a,z)
if(z!=null){y=$.aT[z]
if(y!=null){Object.defineProperty(a,init.dispatchPropertyName,{value:y,enumerable:false,writable:true,configurable:true})
return y.i}x=$.aV[z]
if(x!=null)return x
w=init.interceptorsByTag[z]}}if(w==null)return
x=w.prototype
v=z[0]
if(v==="!"){y=H.bC(x)
$.aT[z]=y
Object.defineProperty(a,init.dispatchPropertyName,{value:y,enumerable:false,writable:true,configurable:true})
return y.i}if(v==="~"){$.aV[z]=x
return x}if(v==="-"){u=H.bC(x)
Object.defineProperty(Object.getPrototypeOf(a),init.dispatchPropertyName,{value:u,enumerable:false,writable:true,configurable:true})
return u.i}if(v==="+")return H.d5(a,x)
if(v==="*")throw H.c(new P.cC(z))
if(init.leafTags[z]===true){u=H.bC(x)
Object.defineProperty(Object.getPrototypeOf(a),init.dispatchPropertyName,{value:u,enumerable:false,writable:true,configurable:true})
return u.i}else return H.d5(a,x)},
d5:function(a,b){var z=Object.getPrototypeOf(a)
Object.defineProperty(z,init.dispatchPropertyName,{value:J.aW(b,z,null,null),enumerable:false,writable:true,configurable:true})
return b},
bC:function(a){return J.aW(a,!1,null,!!a.$isaD)},
hk:function(a,b,c){var z=b.prototype
if(init.leafTags[a]===true)return J.aW(z,!1,null,!!z.$isaD)
else return J.aW(z,c,null,null)},
h9:function(){if(!0===$.bB)return
$.bB=!0
H.ha()},
ha:function(){var z,y,x,w,v,u,t,s
$.aT=Object.create(null)
$.aV=Object.create(null)
H.h5()
z=init.interceptorsByTag
y=Object.getOwnPropertyNames(z)
if(typeof window!="undefined"){window
x=function(){}
for(w=0;w<y.length;++w){v=y[w]
u=$.d6.$1(v)
if(u!=null){t=H.hk(v,z[v],u)
if(t!=null){Object.defineProperty(u,init.dispatchPropertyName,{value:t,enumerable:false,writable:true,configurable:true})
x.prototype=u}}}}for(w=0;w<y.length;++w){v=y[w]
if(/^[A-Za-z_]/.test(v)){s=z[v]
z["!"+v]=s
z["~"+v]=s
z["-"+v]=s
z["+"+v]=s
z["*"+v]=s}}},
h5:function(){var z,y,x,w,v,u,t
z=C.t()
z=H.a3(C.u,H.a3(C.v,H.a3(C.j,H.a3(C.j,H.a3(C.x,H.a3(C.w,H.a3(C.y(C.k),z)))))))
if(typeof dartNativeDispatchHooksTransformer!="undefined"){y=dartNativeDispatchHooksTransformer
if(typeof y=="function")y=[y]
if(y.constructor==Array)for(x=0;x<y.length;++x){w=y[x]
if(typeof w=="function")z=w(z)||z}}v=z.getTag
u=z.getUnknownTag
t=z.prototypeForTag
$.bz=new H.h6(v)
$.cT=new H.h7(u)
$.d6=new H.h8(t)},
a3:function(a,b){return a(b)||b},
eu:{"^":"a;a,b,c,d,e,f,r,x",n:{
ev:function(a){var z,y,x
z=a.$reflectionInfo
if(z==null)return
z.fixed$length=Array
z=z
y=z[0]
x=z[1]
return new H.eu(a,z,(y&1)===1,y>>1,x>>1,(x&1)===1,z[2],null)}}},
eT:{"^":"a;a,b,c,d,e,f",
C:function(a){var z,y,x
z=new RegExp(this.a).exec(a)
if(z==null)return
y=Object.create(null)
x=this.b
if(x!==-1)y.arguments=z[x+1]
x=this.c
if(x!==-1)y.argumentsExpr=z[x+1]
x=this.d
if(x!==-1)y.expr=z[x+1]
x=this.e
if(x!==-1)y.method=z[x+1]
x=this.f
if(x!==-1)y.receiver=z[x+1]
return y},
n:{
K:function(a){var z,y,x,w,v,u
a=a.replace(String({}),'$receiver$').replace(/[[\]{}()*+?.\\^$|]/g,"\\$&")
z=a.match(/\\\$[a-zA-Z]+\\\$/g)
if(z==null)z=[]
y=z.indexOf("\\$arguments\\$")
x=z.indexOf("\\$argumentsExpr\\$")
w=z.indexOf("\\$expr\\$")
v=z.indexOf("\\$method\\$")
u=z.indexOf("\\$receiver\\$")
return new H.eT(a.replace(new RegExp('\\\\\\$arguments\\\\\\$','g'),'((?:x|[^x])*)').replace(new RegExp('\\\\\\$argumentsExpr\\\\\\$','g'),'((?:x|[^x])*)').replace(new RegExp('\\\\\\$expr\\\\\\$','g'),'((?:x|[^x])*)').replace(new RegExp('\\\\\\$method\\\\\\$','g'),'((?:x|[^x])*)').replace(new RegExp('\\\\\\$receiver\\\\\\$','g'),'((?:x|[^x])*)'),y,x,w,v,u)},
aL:function(a){return function($expr$){var $argumentsExpr$='$arguments$'
try{$expr$.$method$($argumentsExpr$)}catch(z){return z.message}}(a)},
cx:function(a){return function($expr$){try{$expr$.$method$}catch(z){return z.message}}(a)}}},
cf:{"^":"w;a,b",
i:function(a){var z=this.b
if(z==null)return"NullError: "+H.b(this.a)
return"NullError: method not found: '"+H.b(z)+"' on null"}},
ed:{"^":"w;a,b,c",
i:function(a){var z,y
z=this.b
if(z==null)return"NoSuchMethodError: "+H.b(this.a)
y=this.c
if(y==null)return"NoSuchMethodError: method not found: '"+H.b(z)+"' ("+H.b(this.a)+")"
return"NoSuchMethodError: method not found: '"+H.b(z)+"' on '"+H.b(y)+"' ("+H.b(this.a)+")"},
n:{
ba:function(a,b){var z,y
z=b==null
y=z?null:b.method
return new H.ed(a,y,z?null:b.receiver)}}},
eU:{"^":"w;a",
i:function(a){var z=this.a
return z.length===0?"Error":"Error: "+z}},
hs:{"^":"d:2;a",
$1:function(a){if(!!J.n(a).$isw)if(a.$thrownJsError==null)a.$thrownJsError=this.a
return a}},
cM:{"^":"a;a,b",
i:function(a){var z,y
z=this.b
if(z!=null)return z
z=this.a
y=z!==null&&typeof z==="object"?z.stack:null
z=y==null?"":y
this.b=z
return z}},
hc:{"^":"d:0;a",
$0:function(){return this.a.$0()}},
hd:{"^":"d:0;a,b",
$0:function(){return this.a.$1(this.b)}},
he:{"^":"d:0;a,b,c",
$0:function(){return this.a.$2(this.b,this.c)}},
hf:{"^":"d:0;a,b,c,d",
$0:function(){return this.a.$3(this.b,this.c,this.d)}},
hg:{"^":"d:0;a,b,c,d,e",
$0:function(){return this.a.$4(this.b,this.c,this.d,this.e)}},
d:{"^":"a;",
i:function(a){return"Closure '"+H.ck(this)+"'"},
gbY:function(){return this},
gbY:function(){return this}},
cq:{"^":"d;"},
eD:{"^":"cq;",
i:function(a){var z=this.$static_name
if(z==null)return"Closure of unknown static method"
return"Closure '"+z+"'"}},
b5:{"^":"cq;a,b,c,d",
l:function(a,b){if(b==null)return!1
if(this===b)return!0
if(!(b instanceof H.b5))return!1
return this.a===b.a&&this.b===b.b&&this.c===b.c},
gt:function(a){var z,y
z=this.c
if(z==null)y=H.Q(this.a)
else y=typeof z!=="object"?J.ay(z):H.Q(z)
z=H.Q(this.b)
if(typeof y!=="number")return y.dU()
return(y^z)>>>0},
i:function(a){var z=this.c
if(z==null)z=this.a
return"Closure '"+H.b(this.d)+"' of "+H.aF(z)},
n:{
b6:function(a){return a.a},
bP:function(a){return a.c},
dN:function(){var z=$.aa
if(z==null){z=H.aA("self")
$.aa=z}return z},
aA:function(a){var z,y,x,w,v
z=new H.b5("self","target","receiver","name")
y=Object.getOwnPropertyNames(z)
y.fixed$length=Array
x=y
for(y=x.length,w=0;w<y;++w){v=x[w]
if(z[v]===a)return v}}}},
ew:{"^":"w;a",
i:function(a){return"RuntimeError: "+H.b(this.a)}},
aJ:{"^":"a;"},
ex:{"^":"aJ;a,b,c,d",
O:function(a){var z=this.cE(a)
return z==null?!1:H.d_(z,this.D())},
cE:function(a){var z=J.n(a)
return"$signature" in z?z.$signature():null},
D:function(){var z,y,x,w,v,u,t
z={func:"dynafunc"}
y=this.a
x=J.n(y)
if(!!x.$isis)z.v=true
else if(!x.$isbT)z.ret=y.D()
y=this.b
if(y!=null&&y.length!==0)z.args=H.cn(y)
y=this.c
if(y!=null&&y.length!==0)z.opt=H.cn(y)
y=this.d
if(y!=null){w=Object.create(null)
v=H.cY(y)
for(x=v.length,u=0;u<x;++u){t=v[u]
w[t]=y[t].D()}z.named=w}return z},
i:function(a){var z,y,x,w,v,u,t,s
z=this.b
if(z!=null)for(y=z.length,x="(",w=!1,v=0;v<y;++v,w=!0){u=z[v]
if(w)x+=", "
x+=H.b(u)}else{x="("
w=!1}z=this.c
if(z!=null&&z.length!==0){x=(w?x+", ":x)+"["
for(y=z.length,w=!1,v=0;v<y;++v,w=!0){u=z[v]
if(w)x+=", "
x+=H.b(u)}x+="]"}else{z=this.d
if(z!=null){x=(w?x+", ":x)+"{"
t=H.cY(z)
for(y=t.length,w=!1,v=0;v<y;++v,w=!0){s=t[v]
if(w)x+=", "
x+=H.b(z[s].D())+" "+s}x+="}"}}return x+(") -> "+H.b(this.a))},
n:{
cn:function(a){var z,y,x
a=a
z=[]
for(y=a.length,x=0;x<y;++x)z.push(a[x].D())
return z}}},
bT:{"^":"aJ;",
i:function(a){return"dynamic"},
D:function(){return}},
ez:{"^":"aJ;a",
D:function(){var z,y
z=this.a
y=H.d1(z)
if(y==null)throw H.c("no type for '"+z+"'")
return y},
i:function(a){return this.a}},
ey:{"^":"aJ;a,b,c",
D:function(){var z,y,x,w
z=this.c
if(z!=null)return z
z=this.a
y=[H.d1(z)]
if(0>=y.length)return H.e(y,0)
if(y[0]==null)throw H.c("no type for '"+z+"<...>'")
for(z=this.b,x=z.length,w=0;w<z.length;z.length===x||(0,H.db)(z),++w)y.push(z[w].D())
this.c=y
return y},
i:function(a){var z=this.b
return this.a+"<"+(z&&C.d).dw(z,", ")+">"}},
Z:{"^":"a;a,b,c,d,e,f,r",
gj:function(a){return this.a},
gK:function(a){return this.a===0},
gbE:function(){return H.i(new H.ef(this),[H.F(this,0)])},
gbU:function(a){return H.aE(this.gbE(),new H.ec(this),H.F(this,0),H.F(this,1))},
bx:function(a){var z
if((a&0x3ffffff)===a){z=this.c
if(z==null)return!1
return this.cB(z,a)}else return this.ds(a)},
ds:function(a){var z=this.d
if(z==null)return!1
return this.a8(this.ag(z,this.a7(a)),a)>=0},
h:function(a,b){var z,y,x
if(typeof b==="string"){z=this.b
if(z==null)return
y=this.a1(z,b)
return y==null?null:y.gS()}else if(typeof b==="number"&&(b&0x3ffffff)===b){x=this.c
if(x==null)return
y=this.a1(x,b)
return y==null?null:y.gS()}else return this.dt(b)},
dt:function(a){var z,y,x
z=this.d
if(z==null)return
y=this.ag(z,this.a7(a))
x=this.a8(y,a)
if(x<0)return
return y[x].gS()},
u:function(a,b,c){var z,y,x,w,v,u
if(typeof b==="string"){z=this.b
if(z==null){z=this.aC()
this.b=z}this.b1(z,b,c)}else if(typeof b==="number"&&(b&0x3ffffff)===b){y=this.c
if(y==null){y=this.aC()
this.c=y}this.b1(y,b,c)}else{x=this.d
if(x==null){x=this.aC()
this.d=x}w=this.a7(b)
v=this.ag(x,w)
if(v==null)this.aG(x,w,[this.aD(b,c)])
else{u=this.a8(v,b)
if(u>=0)v[u].sS(c)
else v.push(this.aD(b,c))}}},
a9:function(a,b){if(typeof b==="string")return this.bj(this.b,b)
else if(typeof b==="number"&&(b&0x3ffffff)===b)return this.bj(this.c,b)
else return this.du(b)},
du:function(a){var z,y,x,w
z=this.d
if(z==null)return
y=this.ag(z,this.a7(a))
x=this.a8(y,a)
if(x<0)return
w=y.splice(x,1)[0]
this.bp(w)
return w.gS()},
W:function(a){if(this.a>0){this.f=null
this.e=null
this.d=null
this.c=null
this.b=null
this.a=0
this.r=this.r+1&67108863}},
w:function(a,b){var z,y
z=this.e
y=this.r
for(;z!=null;){b.$2(z.a,z.b)
if(y!==this.r)throw H.c(new P.y(this))
z=z.c}},
b1:function(a,b,c){var z=this.a1(a,b)
if(z==null)this.aG(a,b,this.aD(b,c))
else z.sS(c)},
bj:function(a,b){var z
if(a==null)return
z=this.a1(a,b)
if(z==null)return
this.bp(z)
this.b6(a,b)
return z.gS()},
aD:function(a,b){var z,y
z=new H.ee(a,b,null,null)
if(this.e==null){this.f=z
this.e=z}else{y=this.f
z.d=y
y.c=z
this.f=z}++this.a
this.r=this.r+1&67108863
return z},
bp:function(a){var z,y
z=a.gcL()
y=a.c
if(z==null)this.e=y
else z.c=y
if(y==null)this.f=z
else y.d=z;--this.a
this.r=this.r+1&67108863},
a7:function(a){return J.ay(a)&0x3ffffff},
a8:function(a,b){var z,y
if(a==null)return-1
z=a.length
for(y=0;y<z;++y)if(J.T(a[y].gbD(),b))return y
return-1},
i:function(a){return P.ek(this)},
a1:function(a,b){return a[b]},
ag:function(a,b){return a[b]},
aG:function(a,b,c){a[b]=c},
b6:function(a,b){delete a[b]},
cB:function(a,b){return this.a1(a,b)!=null},
aC:function(){var z=Object.create(null)
this.aG(z,"<non-identifier-key>",z)
this.b6(z,"<non-identifier-key>")
return z},
$isdZ:1},
ec:{"^":"d:2;a",
$1:function(a){return this.a.h(0,a)}},
ee:{"^":"a;bD:a<,S:b@,c,cL:d<"},
ef:{"^":"H;a",
gj:function(a){return this.a.a},
gA:function(a){var z,y
z=this.a
y=new H.eg(z,z.r,null,null)
y.c=z.e
return y},
w:function(a,b){var z,y,x
z=this.a
y=z.e
x=z.r
for(;y!=null;){b.$1(y.a)
if(x!==z.r)throw H.c(new P.y(z))
y=y.c}},
$ist:1},
eg:{"^":"a;a,b,c,d",
gq:function(){return this.d},
p:function(){var z=this.a
if(this.b!==z.r)throw H.c(new P.y(z))
else{z=this.c
if(z==null){this.d=null
return!1}else{this.d=z.a
this.c=z.c
return!0}}}},
h6:{"^":"d:2;a",
$1:function(a){return this.a(a)}},
h7:{"^":"d:6;a",
$2:function(a,b){return this.a(a,b)}},
h8:{"^":"d:7;a",
$1:function(a){return this.a(a)}}}],["","",,H,{"^":"",
cY:function(a){var z=H.i(a?Object.keys(a):[],[null])
z.fixed$length=Array
return z}}],["","",,H,{"^":"",
hm:function(a){if(typeof dartPrint=="function"){dartPrint(a)
return}if(typeof console=="object"&&typeof console.log!="undefined"){console.log(a)
return}if(typeof window=="object")return
if(typeof print=="function"){print(a)
return}throw"Unable to print message: "+String(a)}}],["","",,H,{"^":"",
l:function(a){return a},
cN:function(a){return a},
ca:{"^":"f;",$isca:1,"%":"ArrayBuffer"},
bg:{"^":"f;",$isbg:1,"%":"DataView;ArrayBufferView;be|cb|cd|bf|cc|ce|P"},
be:{"^":"bg;",
gj:function(a){return a.length},
$isaD:1,
$asaD:I.ak,
$isac:1,
$asac:I.ak},
bf:{"^":"cd;",
h:function(a,b){if(b>>>0!==b||b>=a.length)H.r(H.u(a,b))
return a[b]},
u:function(a,b,c){if(b>>>0!==b||b>=a.length)H.r(H.u(a,b))
a[b]=c}},
cb:{"^":"be+c8;",$isk:1,
$ask:function(){return[P.aZ]},
$ist:1},
cd:{"^":"cb+c_;"},
P:{"^":"ce;",
u:function(a,b,c){if(b>>>0!==b||b>=a.length)H.r(H.u(a,b))
a[b]=c},
$isk:1,
$ask:function(){return[P.p]},
$ist:1},
cc:{"^":"be+c8;",$isk:1,
$ask:function(){return[P.p]},
$ist:1},
ce:{"^":"cc+c_;"},
i2:{"^":"bf;",$isk:1,
$ask:function(){return[P.aZ]},
$ist:1,
"%":"Float32Array"},
en:{"^":"bf;",$isk:1,
$ask:function(){return[P.aZ]},
$ist:1,
"%":"Float64Array"},
i3:{"^":"P;",
h:function(a,b){if(b>>>0!==b||b>=a.length)H.r(H.u(a,b))
return a[b]},
$isk:1,
$ask:function(){return[P.p]},
$ist:1,
"%":"Int16Array"},
i4:{"^":"P;",
h:function(a,b){if(b>>>0!==b||b>=a.length)H.r(H.u(a,b))
return a[b]},
$isk:1,
$ask:function(){return[P.p]},
$ist:1,
"%":"Int32Array"},
i5:{"^":"P;",
h:function(a,b){if(b>>>0!==b||b>=a.length)H.r(H.u(a,b))
return a[b]},
$isk:1,
$ask:function(){return[P.p]},
$ist:1,
"%":"Int8Array"},
i6:{"^":"P;",
h:function(a,b){if(b>>>0!==b||b>=a.length)H.r(H.u(a,b))
return a[b]},
$isk:1,
$ask:function(){return[P.p]},
$ist:1,
"%":"Uint16Array"},
i7:{"^":"P;",
h:function(a,b){if(b>>>0!==b||b>=a.length)H.r(H.u(a,b))
return a[b]},
$isk:1,
$ask:function(){return[P.p]},
$ist:1,
"%":"Uint32Array"},
i8:{"^":"P;",
gj:function(a){return a.length},
h:function(a,b){if(b>>>0!==b||b>=a.length)H.r(H.u(a,b))
return a[b]},
$isk:1,
$ask:function(){return[P.p]},
$ist:1,
"%":"CanvasPixelArray|Uint8ClampedArray"},
i9:{"^":"P;",
gj:function(a){return a.length},
h:function(a,b){if(b>>>0!==b||b>=a.length)H.r(H.u(a,b))
return a[b]},
$isk:1,
$ask:function(){return[P.p]},
$ist:1,
"%":";Uint8Array"}}],["","",,P,{"^":"",
eX:function(){var z,y,x
z={}
if(self.scheduleImmediate!=null)return P.fT()
if(self.MutationObserver!=null&&self.document!=null){y=self.document.createElement("div")
x=self.document.createElement("span")
z.a=null
new self.MutationObserver(H.a5(new P.eZ(z),1)).observe(y,{childList:true})
return new P.eY(z,y,x)}else if(self.setImmediate!=null)return P.fU()
return P.fV()},
it:[function(a){++init.globalState.f.b
self.scheduleImmediate(H.a5(new P.f_(a),0))},"$1","fT",2,0,3],
iu:[function(a){++init.globalState.f.b
self.setImmediate(H.a5(new P.f0(a),0))},"$1","fU",2,0,3],
iv:[function(a){P.bk(C.h,a)},"$1","fV",2,0,3],
cO:function(a,b){var z=H.av()
z=H.a4(z,[z,z]).O(a)
if(z){b.toString
return a}else{b.toString
return a}},
fN:function(){var z,y
for(;z=$.a2,z!=null;){$.ai=null
y=z.b
$.a2=y
if(y==null)$.ah=null
z.a.$0()}},
iC:[function(){$.bs=!0
try{P.fN()}finally{$.ai=null
$.bs=!1
if($.a2!=null)$.$get$bl().$1(P.cV())}},"$0","cV",0,0,1],
cS:function(a){var z=new P.cE(a,null)
if($.a2==null){$.ah=z
$.a2=z
if(!$.bs)$.$get$bl().$1(P.cV())}else{$.ah.b=z
$.ah=z}},
fQ:function(a){var z,y,x
z=$.a2
if(z==null){P.cS(a)
$.ai=$.ah
return}y=new P.cE(a,null)
x=$.ai
if(x==null){y.b=z
$.ai=y
$.a2=y}else{y.b=x.b
x.b=y
$.ai=y
if(y.b==null)$.ah=y}},
d8:function(a){var z=$.m
if(C.b===z){P.aQ(null,null,C.b,a)
return}z.toString
P.aQ(null,null,z,z.aI(a,!0))},
fP:function(a,b,c){var z,y,x,w,v,u,t
try{b.$1(a.$0())}catch(u){t=H.G(u)
z=t
y=H.B(u)
$.m.toString
x=null
if(x==null)c.$2(z,y)
else{t=J.a9(x)
w=t
v=x.gH()
c.$2(w,v)}}},
fH:function(a,b,c,d){var z=a.aJ()
if(!!J.n(z).$isY)z.aV(new P.fK(b,c,d))
else b.a0(c,d)},
fI:function(a,b){return new P.fJ(a,b)},
fG:function(a,b,c){$.m.toString
a.ap(b,c)},
eS:function(a,b){var z=$.m
if(z===C.b){z.toString
return P.bk(a,b)}return P.bk(a,z.aI(b,!0))},
bk:function(a,b){var z=C.c.a3(a.a,1000)
return H.eP(z<0?0:z,b)},
at:function(a,b,c,d,e){var z={}
z.a=d
P.fQ(new P.fO(z,e))},
cP:function(a,b,c,d){var z,y
y=$.m
if(y===c)return d.$0()
$.m=c
z=y
try{y=d.$0()
return y}finally{$.m=z}},
cR:function(a,b,c,d,e){var z,y
y=$.m
if(y===c)return d.$1(e)
$.m=c
z=y
try{y=d.$1(e)
return y}finally{$.m=z}},
cQ:function(a,b,c,d,e,f){var z,y
y=$.m
if(y===c)return d.$2(e,f)
$.m=c
z=y
try{y=d.$2(e,f)
return y}finally{$.m=z}},
aQ:function(a,b,c,d){var z=C.b!==c
if(z)d=c.aI(d,!(!z||!1))
P.cS(d)},
eZ:{"^":"d:2;a",
$1:function(a){var z,y;--init.globalState.f.b
z=this.a
y=z.a
z.a=null
y.$0()}},
eY:{"^":"d:8;a,b,c",
$1:function(a){var z,y;++init.globalState.f.b
this.a.a=a
z=this.b
y=this.c
z.firstChild?z.removeChild(y):z.appendChild(y)}},
f_:{"^":"d:0;a",
$0:function(){--init.globalState.f.b
this.a.$0()}},
f0:{"^":"d:0;a",
$0:function(){--init.globalState.f.b
this.a.$0()}},
Y:{"^":"a;"},
f4:{"^":"a;"},
fE:{"^":"f4;a"},
cJ:{"^":"a;aE:a<,b,c,d,e",
gcR:function(){return this.b.b},
gbC:function(){return(this.c&1)!==0},
gdr:function(){return(this.c&2)!==0},
gbB:function(){return this.c===8},
dn:function(a){return this.b.b.aP(this.d,a)},
dB:function(a){if(this.c!==6)return!0
return this.b.b.aP(this.d,J.a9(a))},
dj:function(a){var z,y,x,w
z=this.e
y=H.av()
y=H.a4(y,[y,y]).O(z)
x=J.j(a)
w=this.b
if(y)return w.b.dG(z,x.gR(a),a.gH())
else return w.b.aP(z,x.gR(a))},
dq:function(){return this.b.b.bN(this.d)}},
S:{"^":"a;a2:a@,b,cP:c<",
gcJ:function(){return this.a===2},
gaB:function(){return this.a>=4},
bQ:function(a,b){var z,y
z=$.m
if(z!==C.b){z.toString
if(b!=null)b=P.cO(b,z)}y=H.i(new P.S(0,z,null),[null])
this.aq(new P.cJ(null,y,b==null?1:3,a,b))
return y},
aR:function(a){return this.bQ(a,null)},
aV:function(a){var z,y
z=$.m
y=new P.S(0,z,null)
y.$builtinTypeInfo=this.$builtinTypeInfo
if(z!==C.b)z.toString
this.aq(new P.cJ(null,y,8,a,null))
return y},
aq:function(a){var z,y
z=this.a
if(z<=1){a.a=this.c
this.c=a}else{if(z===2){y=this.c
if(!y.gaB()){y.aq(a)
return}this.a=y.a
this.c=y.c}z=this.b
z.toString
P.aQ(null,null,z,new P.fc(this,a))}},
bi:function(a){var z,y,x,w,v
z={}
z.a=a
if(a==null)return
y=this.a
if(y<=1){x=this.c
this.c=a
if(x!=null){for(w=a;w.gaE()!=null;)w=w.a
w.a=x}}else{if(y===2){v=this.c
if(!v.gaB()){v.bi(a)
return}this.a=v.a
this.c=v.c}z.a=this.ah(a)
y=this.b
y.toString
P.aQ(null,null,y,new P.fh(z,this))}},
aF:function(){var z=this.c
this.c=null
return this.ah(z)},
ah:function(a){var z,y,x
for(z=a,y=null;z!=null;y=z,z=x){x=z.gaE()
z.a=y}return y},
a_:function(a){var z
if(!!J.n(a).$isY)P.cK(a,this)
else{z=this.aF()
this.a=4
this.c=a
P.af(this,z)}},
a0:[function(a,b){var z=this.aF()
this.a=8
this.c=new P.az(a,b)
P.af(this,z)},function(a){return this.a0(a,null)},"dV","$2","$1","gaw",2,2,9,0],
$isY:1,
n:{
fd:function(a,b){var z,y,x,w
b.sa2(1)
try{a.bQ(new P.fe(b),new P.ff(b))}catch(x){w=H.G(x)
z=w
y=H.B(x)
P.d8(new P.fg(b,z,y))}},
cK:function(a,b){var z,y,x
for(;a.gcJ();)a=a.c
z=a.gaB()
y=b.c
if(z){b.c=null
x=b.ah(y)
b.a=a.a
b.c=a.c
P.af(b,x)}else{b.a=2
b.c=a
a.bi(y)}},
af:function(a,b){var z,y,x,w,v,u,t,s,r,q,p,o
z={}
z.a=a
for(y=a;!0;){x={}
w=y.a===8
if(b==null){if(w){v=y.c
z=y.b
y=J.a9(v)
x=v.gH()
z.toString
P.at(null,null,z,y,x)}return}for(;b.gaE()!=null;b=u){u=b.a
b.a=null
P.af(z.a,b)}t=z.a.c
x.a=w
x.b=t
y=!w
if(!y||b.gbC()||b.gbB()){s=b.gcR()
if(w){r=z.a.b
r.toString
r=r==null?s==null:r===s
if(!r)s.toString
else r=!0
r=!r}else r=!1
if(r){y=z.a
v=y.c
y=y.b
x=J.a9(v)
r=v.gH()
y.toString
P.at(null,null,y,x,r)
return}q=$.m
if(q==null?s!=null:q!==s)$.m=s
else q=null
if(b.gbB())new P.fk(z,x,w,b).$0()
else if(y){if(b.gbC())new P.fj(x,b,t).$0()}else if(b.gdr())new P.fi(z,x,b).$0()
if(q!=null)$.m=q
y=x.b
r=J.n(y)
if(!!r.$isY){p=b.b
if(!!r.$isS)if(y.a>=4){o=p.c
p.c=null
b=p.ah(o)
p.a=y.a
p.c=y.c
z.a=y
continue}else P.cK(y,p)
else P.fd(y,p)
return}}p=b.b
b=p.aF()
y=x.a
x=x.b
if(!y){p.a=4
p.c=x}else{p.a=8
p.c=x}z.a=p
y=p}}}},
fc:{"^":"d:0;a,b",
$0:function(){P.af(this.a,this.b)}},
fh:{"^":"d:0;a,b",
$0:function(){P.af(this.b,this.a.a)}},
fe:{"^":"d:2;a",
$1:function(a){var z=this.a
z.a=0
z.a_(a)}},
ff:{"^":"d:10;a",
$2:function(a,b){this.a.a0(a,b)},
$1:function(a){return this.$2(a,null)}},
fg:{"^":"d:0;a,b,c",
$0:function(){this.a.a0(this.b,this.c)}},
fk:{"^":"d:1;a,b,c,d",
$0:function(){var z,y,x,w,v,u,t
z=null
try{z=this.d.dq()}catch(w){v=H.G(w)
y=v
x=H.B(w)
if(this.c){v=J.a9(this.a.a.c)
u=y
u=v==null?u==null:v===u
v=u}else v=!1
u=this.b
if(v)u.b=this.a.a.c
else u.b=new P.az(y,x)
u.a=!0
return}if(!!J.n(z).$isY){if(z instanceof P.S&&z.ga2()>=4){if(z.ga2()===8){v=this.b
v.b=z.gcP()
v.a=!0}return}t=this.a.a
v=this.b
v.b=z.aR(new P.fl(t))
v.a=!1}}},
fl:{"^":"d:2;a",
$1:function(a){return this.a}},
fj:{"^":"d:1;a,b,c",
$0:function(){var z,y,x,w
try{this.a.b=this.b.dn(this.c)}catch(x){w=H.G(x)
z=w
y=H.B(x)
w=this.a
w.b=new P.az(z,y)
w.a=!0}}},
fi:{"^":"d:1;a,b,c",
$0:function(){var z,y,x,w,v,u,t,s
try{z=this.a.a.c
w=this.c
if(w.dB(z)===!0&&w.e!=null){v=this.b
v.b=w.dj(z)
v.a=!1}}catch(u){w=H.G(u)
y=w
x=H.B(u)
w=this.a
v=J.a9(w.a.c)
t=y
s=this.b
if(v==null?t==null:v===t)s.b=w.a.c
else s.b=new P.az(y,x)
s.a=!0}}},
cE:{"^":"a;a,b"},
R:{"^":"a;",
Y:function(a,b){return H.i(new P.fv(b,this),[H.A(this,"R",0),null])},
w:function(a,b){var z,y
z={}
y=H.i(new P.S(0,$.m,null),[null])
z.a=null
z.a=this.X(new P.eH(z,this,b,y),!0,new P.eI(y),y.gaw())
return y},
gj:function(a){var z,y
z={}
y=H.i(new P.S(0,$.m,null),[P.p])
z.a=0
this.X(new P.eJ(z),!0,new P.eK(z,y),y.gaw())
return y},
aS:function(a){var z,y
z=H.i([],[H.A(this,"R",0)])
y=H.i(new P.S(0,$.m,null),[[P.k,H.A(this,"R",0)]])
this.X(new P.eL(this,z),!0,new P.eM(z,y),y.gaw())
return y}},
eH:{"^":"d;a,b,c,d",
$1:function(a){P.fP(new P.eF(this.c,a),new P.eG(),P.fI(this.a.a,this.d))},
$signature:function(){return H.bv(function(a){return{func:1,args:[a]}},this.b,"R")}},
eF:{"^":"d:0;a,b",
$0:function(){return this.a.$1(this.b)}},
eG:{"^":"d:2;",
$1:function(a){}},
eI:{"^":"d:0;a",
$0:function(){this.a.a_(null)}},
eJ:{"^":"d:2;a",
$1:function(a){++this.a.a}},
eK:{"^":"d:0;a,b",
$0:function(){this.b.a_(this.a.a)}},
eL:{"^":"d;a,b",
$1:function(a){this.b.push(a)},
$signature:function(){return H.bv(function(a){return{func:1,args:[a]}},this.a,"R")}},
eM:{"^":"d:0;a,b",
$0:function(){this.b.a_(this.a)}},
eE:{"^":"a;"},
iw:{"^":"a;"},
f1:{"^":"a;a2:e@",
aM:function(a,b){var z=this.e
if((z&8)!==0)return
this.e=(z+128|4)>>>0
if(z<128&&this.r!=null)this.r.bv()
if((z&4)===0&&(this.e&32)===0)this.b9(this.gbe())},
bJ:function(a){return this.aM(a,null)},
bL:function(){var z=this.e
if((z&8)!==0)return
if(z>=128){z-=128
this.e=z
if(z<128){if((z&64)!==0){z=this.r
z=!z.gK(z)}else z=!1
if(z)this.r.am(this)
else{z=(this.e&4294967291)>>>0
this.e=z
if((z&32)===0)this.b9(this.gbg())}}}},
aJ:function(){var z=(this.e&4294967279)>>>0
this.e=z
if((z&8)!==0)return this.f
this.at()
return this.f},
at:function(){var z=(this.e|8)>>>0
this.e=z
if((z&64)!==0)this.r.bv()
if((this.e&32)===0)this.r=null
this.f=this.bd()},
as:["cm",function(a){var z=this.e
if((z&8)!==0)return
if(z<32)this.bl(a)
else this.ar(H.i(new P.f5(a,null),[null]))}],
ap:["cn",function(a,b){var z=this.e
if((z&8)!==0)return
if(z<32)this.bn(a,b)
else this.ar(new P.f7(a,b,null))}],
cv:function(){var z=this.e
if((z&8)!==0)return
z=(z|2)>>>0
this.e=z
if(z<32)this.bm()
else this.ar(C.o)},
bf:[function(){},"$0","gbe",0,0,1],
bh:[function(){},"$0","gbg",0,0,1],
bd:function(){return},
ar:function(a){var z,y
z=this.r
if(z==null){z=H.i(new P.fD(null,null,0),[null])
this.r=z}z.v(0,a)
y=this.e
if((y&64)===0){y=(y|64)>>>0
this.e=y
if(y<128)this.r.am(this)}},
bl:function(a){var z=this.e
this.e=(z|32)>>>0
this.d.aQ(this.a,a)
this.e=(this.e&4294967263)>>>0
this.au((z&4)!==0)},
bn:function(a,b){var z,y
z=this.e
y=new P.f3(this,a,b)
if((z&1)!==0){this.e=(z|16)>>>0
this.at()
z=this.f
if(!!J.n(z).$isY)z.aV(y)
else y.$0()}else{y.$0()
this.au((z&4)!==0)}},
bm:function(){var z,y
z=new P.f2(this)
this.at()
this.e=(this.e|16)>>>0
y=this.f
if(!!J.n(y).$isY)y.aV(z)
else z.$0()},
b9:function(a){var z=this.e
this.e=(z|32)>>>0
a.$0()
this.e=(this.e&4294967263)>>>0
this.au((z&4)!==0)},
au:function(a){var z,y
if((this.e&64)!==0){z=this.r
z=z.gK(z)}else z=!1
if(z){z=(this.e&4294967231)>>>0
this.e=z
if((z&4)!==0)if(z<128){z=this.r
z=z==null||z.gK(z)}else z=!1
else z=!1
if(z)this.e=(this.e&4294967291)>>>0}for(;!0;a=y){z=this.e
if((z&8)!==0){this.r=null
return}y=(z&4)!==0
if(a===y)break
this.e=(z^32)>>>0
if(y)this.bf()
else this.bh()
this.e=(this.e&4294967263)>>>0}z=this.e
if((z&64)!==0&&z<128)this.r.am(this)},
cr:function(a,b,c,d){var z=this.d
z.toString
this.a=a
this.b=P.cO(b,z)
this.c=c}},
f3:{"^":"d:1;a,b,c",
$0:function(){var z,y,x,w,v,u
z=this.a
y=z.e
if((y&8)!==0&&(y&16)===0)return
z.e=(y|32)>>>0
y=z.b
x=H.a4(H.av(),[H.cW(P.a),H.cW(P.a0)]).O(y)
w=z.d
v=this.b
u=z.b
if(x)w.dH(u,v,this.c)
else w.aQ(u,v)
z.e=(z.e&4294967263)>>>0}},
f2:{"^":"d:1;a",
$0:function(){var z,y
z=this.a
y=z.e
if((y&16)===0)return
z.e=(y|42)>>>0
z.d.bO(z.c)
z.e=(z.e&4294967263)>>>0}},
cG:{"^":"a;aj:a@"},
f5:{"^":"cG;b,a",
aN:function(a){a.bl(this.b)}},
f7:{"^":"cG;R:b>,H:c<,a",
aN:function(a){a.bn(this.b,this.c)}},
f6:{"^":"a;",
aN:function(a){a.bm()},
gaj:function(){return},
saj:function(a){throw H.c(new P.aK("No events after a done."))}},
fx:{"^":"a;a2:a@",
am:function(a){var z=this.a
if(z===1)return
if(z>=1){this.a=1
return}P.d8(new P.fy(this,a))
this.a=1},
bv:function(){if(this.a===1)this.a=3}},
fy:{"^":"d:0;a,b",
$0:function(){var z,y,x,w
z=this.a
y=z.a
z.a=0
if(y===3)return
x=z.b
w=x.gaj()
z.b=w
if(w==null)z.c=null
x.aN(this.b)}},
fD:{"^":"fx;b,c,a",
gK:function(a){return this.c==null},
v:function(a,b){var z=this.c
if(z==null){this.c=b
this.b=b}else{z.saj(b)
this.c=b}}},
fK:{"^":"d:0;a,b,c",
$0:function(){return this.a.a0(this.b,this.c)}},
fJ:{"^":"d:11;a,b",
$2:function(a,b){P.fH(this.a,this.b,a,b)}},
bn:{"^":"R;",
X:function(a,b,c,d){return this.cC(a,d,c,!0===b)},
bG:function(a,b,c){return this.X(a,null,b,c)},
cC:function(a,b,c,d){return P.fb(this,a,b,c,d,H.A(this,"bn",0),H.A(this,"bn",1))},
ba:function(a,b){b.as(a)},
cI:function(a,b,c){c.ap(a,b)},
$asR:function(a,b){return[b]}},
cI:{"^":"f1;x,y,a,b,c,d,e,f,r",
as:function(a){if((this.e&2)!==0)return
this.cm(a)},
ap:function(a,b){if((this.e&2)!==0)return
this.cn(a,b)},
bf:[function(){var z=this.y
if(z==null)return
z.bJ(0)},"$0","gbe",0,0,1],
bh:[function(){var z=this.y
if(z==null)return
z.bL()},"$0","gbg",0,0,1],
bd:function(){var z=this.y
if(z!=null){this.y=null
return z.aJ()}return},
dW:[function(a){this.x.ba(a,this)},"$1","gcF",2,0,function(){return H.bv(function(a,b){return{func:1,v:true,args:[a]}},this.$receiver,"cI")}],
dY:[function(a,b){this.x.cI(a,b,this)},"$2","gcH",4,0,12],
dX:[function(){this.cv()},"$0","gcG",0,0,1],
cs:function(a,b,c,d,e,f,g){var z,y
z=this.gcF()
y=this.gcH()
this.y=this.x.a.bG(z,this.gcG(),y)},
n:{
fb:function(a,b,c,d,e,f,g){var z=$.m
z=H.i(new P.cI(a,null,null,null,null,z,e?1:0,null,null),[f,g])
z.cr(b,c,d,e)
z.cs(a,b,c,d,e,f,g)
return z}}},
fv:{"^":"bn;b,a",
ba:function(a,b){var z,y,x,w,v
z=null
try{z=this.b.$1(a)}catch(w){v=H.G(w)
y=v
x=H.B(w)
P.fG(b,y,x)
return}b.as(z)}},
az:{"^":"a;R:a>,H:b<",
i:function(a){return H.b(this.a)},
$isw:1},
fF:{"^":"a;"},
fO:{"^":"d:0;a,b",
$0:function(){var z,y,x
z=this.a
y=z.a
if(y==null){x=new P.cg()
z.a=x
z=x}else z=y
y=this.b
if(y==null)throw H.c(z)
x=H.c(z)
x.stack=J.U(y)
throw x}},
fz:{"^":"fF;",
bO:function(a){var z,y,x,w
try{if(C.b===$.m){x=a.$0()
return x}x=P.cP(null,null,this,a)
return x}catch(w){x=H.G(w)
z=x
y=H.B(w)
return P.at(null,null,this,z,y)}},
aQ:function(a,b){var z,y,x,w
try{if(C.b===$.m){x=a.$1(b)
return x}x=P.cR(null,null,this,a,b)
return x}catch(w){x=H.G(w)
z=x
y=H.B(w)
return P.at(null,null,this,z,y)}},
dH:function(a,b,c){var z,y,x,w
try{if(C.b===$.m){x=a.$2(b,c)
return x}x=P.cQ(null,null,this,a,b,c)
return x}catch(w){x=H.G(w)
z=x
y=H.B(w)
return P.at(null,null,this,z,y)}},
aI:function(a,b){if(b)return new P.fA(this,a)
else return new P.fB(this,a)},
cX:function(a,b){return new P.fC(this,a)},
h:function(a,b){return},
bN:function(a){if($.m===C.b)return a.$0()
return P.cP(null,null,this,a)},
aP:function(a,b){if($.m===C.b)return a.$1(b)
return P.cR(null,null,this,a,b)},
dG:function(a,b,c){if($.m===C.b)return a.$2(b,c)
return P.cQ(null,null,this,a,b,c)}},
fA:{"^":"d:0;a,b",
$0:function(){return this.a.bO(this.b)}},
fB:{"^":"d:0;a,b",
$0:function(){return this.a.bN(this.b)}},
fC:{"^":"d:2;a,b",
$1:function(a){return this.a.aQ(this.b,a)}}}],["","",,P,{"^":"",
eh:function(){return H.i(new H.Z(0,null,null,null,null,null,0),[null,null])},
a_:function(a){return H.fZ(a,H.i(new H.Z(0,null,null,null,null,null,0),[null,null]))},
e6:function(a,b,c){var z,y
if(P.bt(a)){if(b==="("&&c===")")return"(...)"
return b+"..."+c}z=[]
y=$.$get$aj()
y.push(a)
try{P.fM(a,z)}finally{if(0>=y.length)return H.e(y,-1)
y.pop()}y=P.cp(b,z,", ")+c
return y.charCodeAt(0)==0?y:y},
aB:function(a,b,c){var z,y,x
if(P.bt(a))return b+"..."+c
z=new P.bj(b)
y=$.$get$aj()
y.push(a)
try{x=z
x.a=P.cp(x.gV(),a,", ")}finally{if(0>=y.length)return H.e(y,-1)
y.pop()}y=z
y.a=y.gV()+c
y=z.gV()
return y.charCodeAt(0)==0?y:y},
bt:function(a){var z,y
for(z=0;y=$.$get$aj(),z<y.length;++z)if(a===y[z])return!0
return!1},
fM:function(a,b){var z,y,x,w,v,u,t,s,r,q
z=a.gA(a)
y=0
x=0
while(!0){if(!(y<80||x<3))break
if(!z.p())return
w=H.b(z.gq())
b.push(w)
y+=w.length+2;++x}if(!z.p()){if(x<=5)return
if(0>=b.length)return H.e(b,-1)
v=b.pop()
if(0>=b.length)return H.e(b,-1)
u=b.pop()}else{t=z.gq();++x
if(!z.p()){if(x<=4){b.push(H.b(t))
return}v=H.b(t)
if(0>=b.length)return H.e(b,-1)
u=b.pop()
y+=v.length+2}else{s=z.gq();++x
for(;z.p();t=s,s=r){r=z.gq();++x
if(x>100){while(!0){if(!(y>75&&x>3))break
if(0>=b.length)return H.e(b,-1)
y-=b.pop().length+2;--x}b.push("...")
return}}u=H.b(t)
v=H.b(s)
y+=v.length+u.length+4}}if(x>b.length+2){y+=5
q="..."}else q=null
while(!0){if(!(y>80&&b.length>3))break
if(0>=b.length)return H.e(b,-1)
y-=b.pop().length+2
if(q==null){y+=5
q="..."}}if(q!=null)b.push(q)
b.push(u)
b.push(v)},
ad:function(a,b,c,d){return H.i(new P.fp(0,null,null,null,null,null,0),[d])},
ek:function(a){var z,y,x
z={}
if(P.bt(a))return"{...}"
y=new P.bj("")
try{$.$get$aj().push(a)
x=y
x.a=x.gV()+"{"
z.a=!0
J.dv(a,new P.el(z,y))
z=y
z.a=z.gV()+"}"}finally{z=$.$get$aj()
if(0>=z.length)return H.e(z,-1)
z.pop()}z=y.gV()
return z.charCodeAt(0)==0?z:z},
cL:{"^":"Z;a,b,c,d,e,f,r",
a7:function(a){return H.hl(a)&0x3ffffff},
a8:function(a,b){var z,y,x
if(a==null)return-1
z=a.length
for(y=0;y<z;++y){x=a[y].gbD()
if(x==null?b==null:x===b)return y}return-1},
n:{
ag:function(a,b){return H.i(new P.cL(0,null,null,null,null,null,0),[a,b])}}},
fp:{"^":"fm;a,b,c,d,e,f,r",
gA:function(a){var z=new P.bp(this,this.r,null,null)
z.c=this.e
return z},
gj:function(a){return this.a},
d2:function(a,b){var z,y
if(typeof b==="string"&&b!=="__proto__"){z=this.b
if(z==null)return!1
return z[b]!=null}else if(typeof b==="number"&&(b&0x3ffffff)===b){y=this.c
if(y==null)return!1
return y[b]!=null}else return this.cA(b)},
cA:function(a){var z=this.d
if(z==null)return!1
return this.af(z[this.ae(a)],a)>=0},
bH:function(a){var z
if(!(typeof a==="string"&&a!=="__proto__"))z=typeof a==="number"&&(a&0x3ffffff)===a
else z=!0
if(z)return this.d2(0,a)?a:null
else return this.cK(a)},
cK:function(a){var z,y,x
z=this.d
if(z==null)return
y=z[this.ae(a)]
x=this.af(y,a)
if(x<0)return
return J.dg(y,x).gb7()},
w:function(a,b){var z,y
z=this.e
y=this.r
for(;z!=null;){b.$1(z.a)
if(y!==this.r)throw H.c(new P.y(this))
z=z.b}},
v:function(a,b){var z,y
if(typeof b==="string"&&b!=="__proto__"){z=this.b
if(z==null){z=P.bq()
this.b=z}return this.b3(z,b)}else if(typeof b==="number"&&(b&0x3ffffff)===b){y=this.c
if(y==null){y=P.bq()
this.c=y}return this.b3(y,b)}else return this.I(b)},
I:function(a){var z,y,x
z=this.d
if(z==null){z=P.bq()
this.d=z}y=this.ae(a)
x=z[y]
if(x==null)z[y]=[this.av(a)]
else{if(this.af(x,a)>=0)return!1
x.push(this.av(a))}return!0},
a9:function(a,b){if(typeof b==="string"&&b!=="__proto__")return this.b4(this.b,b)
else if(typeof b==="number"&&(b&0x3ffffff)===b)return this.b4(this.c,b)
else return this.cM(b)},
cM:function(a){var z,y,x
z=this.d
if(z==null)return!1
y=z[this.ae(a)]
x=this.af(y,a)
if(x<0)return!1
this.b5(y.splice(x,1)[0])
return!0},
W:function(a){if(this.a>0){this.f=null
this.e=null
this.d=null
this.c=null
this.b=null
this.a=0
this.r=this.r+1&67108863}},
b3:function(a,b){if(a[b]!=null)return!1
a[b]=this.av(b)
return!0},
b4:function(a,b){var z
if(a==null)return!1
z=a[b]
if(z==null)return!1
this.b5(z)
delete a[b]
return!0},
av:function(a){var z,y
z=new P.fq(a,null,null)
if(this.e==null){this.f=z
this.e=z}else{y=this.f
z.c=y
y.b=z
this.f=z}++this.a
this.r=this.r+1&67108863
return z},
b5:function(a){var z,y
z=a.gcz()
y=a.b
if(z==null)this.e=y
else z.b=y
if(y==null)this.f=z
else y.c=z;--this.a
this.r=this.r+1&67108863},
ae:function(a){return J.ay(a)&0x3ffffff},
af:function(a,b){var z,y
if(a==null)return-1
z=a.length
for(y=0;y<z;++y)if(J.T(a[y].gb7(),b))return y
return-1},
$ist:1,
n:{
bq:function(){var z=Object.create(null)
z["<non-identifier-key>"]=z
delete z["<non-identifier-key>"]
return z}}},
fq:{"^":"a;b7:a<,b,cz:c<"},
bp:{"^":"a;a,b,c,d",
gq:function(){return this.d},
p:function(){var z=this.a
if(this.b!==z.r)throw H.c(new P.y(z))
else{z=this.c
if(z==null){this.d=null
return!1}else{this.d=z.a
this.c=z.b
return!0}}}},
fm:{"^":"eA;"},
c8:{"^":"a;",
gA:function(a){return new H.c7(a,this.gj(a),0,null)},
J:function(a,b){return this.h(a,b)},
w:function(a,b){var z,y,x,w
z=this.gj(a)
for(y=a.length,x=z!==y,w=0;w<z;++w){if(w>=y)return H.e(a,w)
b.$1(a[w])
if(x)throw H.c(new P.y(a))}},
Y:function(a,b){return H.i(new H.bd(a,b),[null,null])},
di:function(a,b,c){var z,y,x,w,v
z=this.gj(a)
for(y=a.length,x=z!==y,w=b,v=0;v<z;++v){if(v>=y)return H.e(a,v)
w=c.$2(w,a[v])
if(x)throw H.c(new P.y(a))}return w},
i:function(a){return P.aB(a,"[","]")},
$isk:1,
$ask:null,
$ist:1},
el:{"^":"d:4;a,b",
$2:function(a,b){var z,y
z=this.a
if(!z.a)this.b.a+=", "
z.a=!1
z=this.b
y=z.a+=H.b(a)
z.a=y+": "
z.a+=H.b(b)}},
ei:{"^":"aq;a,b,c,d",
gA:function(a){return new P.fr(this,this.c,this.d,this.b,null)},
w:function(a,b){var z,y,x
z=this.d
for(y=this.b;y!==this.c;y=(y+1&this.a.length-1)>>>0){x=this.a
if(y<0||y>=x.length)return H.e(x,y)
b.$1(x[y])
if(z!==this.d)H.r(new P.y(this))}},
gK:function(a){return this.b===this.c},
gj:function(a){return(this.c-this.b&this.a.length-1)>>>0},
J:function(a,b){var z,y,x,w
z=(this.c-this.b&this.a.length-1)>>>0
if(0>b||b>=z)H.r(P.b8(b,this,"index",null,z))
y=this.a
x=y.length
w=(this.b+b&x-1)>>>0
if(w<0||w>=x)return H.e(y,w)
return y[w]},
W:function(a){var z,y,x,w,v
z=this.b
y=this.c
if(z!==y){for(x=this.a,w=x.length,v=w-1;z!==y;z=(z+1&v)>>>0){if(z<0||z>=w)return H.e(x,z)
x[z]=null}this.c=0
this.b=0;++this.d}},
i:function(a){return P.aB(this,"{","}")},
bK:function(){var z,y,x,w
z=this.b
if(z===this.c)throw H.c(H.c4());++this.d
y=this.a
x=y.length
if(z>=x)return H.e(y,z)
w=y[z]
y[z]=null
this.b=(z+1&x-1)>>>0
return w},
I:function(a){var z,y,x
z=this.a
y=this.c
x=z.length
if(y>=x)return H.e(z,y)
z[y]=a
x=(y+1&x-1)>>>0
this.c=x
if(this.b===x)this.b8();++this.d},
b8:function(){var z,y,x,w
z=new Array(this.a.length*2)
z.fixed$length=Array
y=H.i(z,[H.F(this,0)])
z=this.a
x=this.b
w=z.length-x
C.d.aZ(y,0,w,z,x)
C.d.aZ(y,w,w+this.b,this.a,0)
this.b=0
this.c=this.a.length
this.a=y},
co:function(a,b){var z=new Array(8)
z.fixed$length=Array
this.a=H.i(z,[b])},
$ist:1,
n:{
bb:function(a,b){var z=H.i(new P.ei(null,0,0,0),[b])
z.co(a,b)
return z}}},
fr:{"^":"a;a,b,c,d,e",
gq:function(){return this.e},
p:function(){var z,y,x
z=this.a
if(this.c!==z.d)H.r(new P.y(z))
y=this.d
if(y===this.b){this.e=null
return!1}z=z.a
x=z.length
if(y>=x)return H.e(z,y)
this.e=z[y]
this.d=(y+1&x-1)>>>0
return!0}},
eB:{"^":"a;",
Y:function(a,b){return H.i(new H.bU(this,b),[H.F(this,0),null])},
i:function(a){return P.aB(this,"{","}")},
w:function(a,b){var z
for(z=new P.bp(this,this.r,null,null),z.c=this.e;z.p();)b.$1(z.d)},
$ist:1},
eA:{"^":"eB;"}}],["","",,P,{"^":"",
bW:function(a){if(typeof a==="number"||typeof a==="boolean"||null==a)return J.U(a)
if(typeof a==="string")return JSON.stringify(a)
return P.dW(a)},
dW:function(a){var z=J.n(a)
if(!!z.$isd)return z.i(a)
return H.aF(a)},
E:function(a){return new P.fa(a)},
bc:function(a,b,c){var z,y
z=H.i([],[c])
for(y=J.b0(a);y.p();)z.push(y.gq())
return z},
bD:function(a){var z=H.b(a)
H.hm(z)},
fW:{"^":"a;"},
"+bool":0,
hy:{"^":"a;"},
aZ:{"^":"a6;"},
"+double":0,
ab:{"^":"a;ax:a<",
k:function(a,b){return new P.ab(C.c.k(this.a,b.gax()))},
Z:function(a,b){return new P.ab(this.a-b.gax())},
L:function(a,b){return new P.ab(C.a.bM(this.a*b))},
al:function(a,b){return C.c.al(this.a,b.gax())},
l:function(a,b){if(b==null)return!1
if(!(b instanceof P.ab))return!1
return this.a===b.a},
gt:function(a){return this.a&0x1FFFFFFF},
i:function(a){var z,y,x,w,v
z=new P.dV()
y=this.a
if(y<0)return"-"+new P.ab(-y).i(0)
x=z.$1(C.c.aO(C.c.a3(y,6e7),60))
w=z.$1(C.c.aO(C.c.a3(y,1e6),60))
v=new P.dU().$1(C.c.aO(y,1e6))
return""+C.c.a3(y,36e8)+":"+H.b(x)+":"+H.b(w)+"."+H.b(v)}},
dU:{"^":"d:5;",
$1:function(a){if(a>=1e5)return""+a
if(a>=1e4)return"0"+a
if(a>=1000)return"00"+a
if(a>=100)return"000"+a
if(a>=10)return"0000"+a
return"00000"+a}},
dV:{"^":"d:5;",
$1:function(a){if(a>=10)return""+a
return"0"+a}},
w:{"^":"a;",
gH:function(){return H.B(this.$thrownJsError)}},
cg:{"^":"w;",
i:function(a){return"Throw of null."}},
V:{"^":"w;a,b,c,d",
gaz:function(){return"Invalid argument"+(!this.a?"(s)":"")},
gay:function(){return""},
i:function(a){var z,y,x,w,v,u
z=this.c
y=z!=null?" ("+H.b(z)+")":""
z=this.d
x=z==null?"":": "+H.b(z)
w=this.gaz()+y+x
if(!this.a)return w
v=this.gay()
u=P.bW(this.b)
return w+v+": "+H.b(u)},
n:{
b4:function(a){return new P.V(!1,null,null,a)},
bN:function(a,b,c){return new P.V(!0,a,b,c)}}},
bi:{"^":"V;e,f,a,b,c,d",
gaz:function(){return"RangeError"},
gay:function(){var z,y,x
z=this.e
if(z==null){z=this.f
y=z!=null?": Not less than or equal to "+H.b(z):""}else{x=this.f
if(x==null)y=": Not greater than or equal to "+H.b(z)
else{if(typeof x!=="number")return x.dS()
if(typeof z!=="number")return H.x(z)
if(x>z)y=": Not in range "+z+".."+x+", inclusive"
else y=x<z?": Valid value range is empty":": Only valid value is "+z}}return y},
n:{
es:function(a){return new P.bi(null,null,!1,null,null,a)},
aH:function(a,b,c){return new P.bi(null,null,!0,a,b,"Value not in range")},
aG:function(a,b,c,d,e){return new P.bi(b,c,!0,a,d,"Invalid value")},
cm:function(a,b,c,d,e,f){if(0>a||a>c)throw H.c(P.aG(a,0,c,"start",f))
if(a>b||b>c)throw H.c(P.aG(b,a,c,"end",f))
return b}}},
dY:{"^":"V;e,j:f>,a,b,c,d",
gaz:function(){return"RangeError"},
gay:function(){if(J.df(this.b,0))return": index must not be negative"
var z=this.f
if(z===0)return": no indices are valid"
return": index should be less than "+H.b(z)},
n:{
b8:function(a,b,c,d,e){var z=e!=null?e:J.al(b)
return new P.dY(b,z,!0,a,c,"Index out of range")}}},
N:{"^":"w;a",
i:function(a){return"Unsupported operation: "+this.a}},
cC:{"^":"w;a",
i:function(a){var z=this.a
return z!=null?"UnimplementedError: "+H.b(z):"UnimplementedError"}},
aK:{"^":"w;a",
i:function(a){return"Bad state: "+this.a}},
y:{"^":"w;a",
i:function(a){var z=this.a
if(z==null)return"Concurrent modification during iteration."
return"Concurrent modification during iteration: "+H.b(P.bW(z))+"."}},
ep:{"^":"a;",
i:function(a){return"Out of Memory"},
gH:function(){return},
$isw:1},
co:{"^":"a;",
i:function(a){return"Stack Overflow"},
gH:function(){return},
$isw:1},
dT:{"^":"w;a",
i:function(a){return"Reading static variable '"+this.a+"' during its initialization"}},
fa:{"^":"a;a",
i:function(a){var z=this.a
if(z==null)return"Exception"
return"Exception: "+H.b(z)}},
dX:{"^":"a;a,b",
i:function(a){return"Expando:"+H.b(this.a)},
h:function(a,b){var z,y
z=this.b
if(typeof z!=="string"){if(b==null||typeof b==="boolean"||typeof b==="number"||typeof b==="string")H.r(P.bN(b,"Expandos are not allowed on strings, numbers, booleans or null",null))
return z.get(b)}y=H.bh(b,"expando$values")
return y==null?null:H.bh(y,z)},
u:function(a,b,c){var z,y
z=this.b
if(typeof z!=="string")z.set(b,c)
else{y=H.bh(b,"expando$values")
if(y==null){y=new P.a()
H.cl(b,"expando$values",y)}H.cl(y,z,c)}}},
p:{"^":"a6;"},
"+int":0,
H:{"^":"a;",
Y:function(a,b){return H.aE(this,b,H.A(this,"H",0),null)},
w:function(a,b){var z
for(z=this.gA(this);z.p();)b.$1(z.gq())},
aT:function(a,b){return P.bc(this,!0,H.A(this,"H",0))},
aS:function(a){return this.aT(a,!0)},
gj:function(a){var z,y
z=this.gA(this)
for(y=0;z.p();)++y
return y},
J:function(a,b){var z,y,x
if(b<0)H.r(P.aG(b,0,null,"index",null))
for(z=this.gA(this),y=0;z.p();){x=z.gq()
if(b===y)return x;++y}throw H.c(P.b8(b,this,"index",null,y))},
i:function(a){return P.e6(this,"(",")")}},
e8:{"^":"a;"},
k:{"^":"a;",$ask:null,$ist:1},
"+List":0,
ib:{"^":"a;",
i:function(a){return"null"}},
"+Null":0,
a6:{"^":"a;"},
"+num":0,
a:{"^":";",
l:function(a,b){return this===b},
gt:function(a){return H.Q(this)},
i:function(a){return H.aF(this)},
toString:function(){return this.i(this)}},
a0:{"^":"a;"},
M:{"^":"a;"},
"+String":0,
bj:{"^":"a;V:a<",
gj:function(a){return this.a.length},
i:function(a){var z=this.a
return z.charCodeAt(0)==0?z:z},
n:{
cp:function(a,b,c){var z=J.b0(b)
if(!z.p())return a
if(c.length===0){do a+=H.b(z.gq())
while(z.p())}else{a+=H.b(z.gq())
for(;z.p();)a=a+c+H.b(z.gq())}return a}}}}],["","",,W,{"^":"",
c1:function(a,b,c){var z,y
z=document
y=z.createElement("img")
J.dD(y,b)
return y},
au:function(a){var z=$.m
if(z===C.b)return a
return z.cX(a,!0)},
v:{"^":"bV;","%":"HTMLAppletElement|HTMLBRElement|HTMLBaseElement|HTMLButtonElement|HTMLContentElement|HTMLDListElement|HTMLDataListElement|HTMLDetailsElement|HTMLDialogElement|HTMLDirectoryElement|HTMLDivElement|HTMLFieldSetElement|HTMLFontElement|HTMLFrameElement|HTMLHRElement|HTMLHeadElement|HTMLHeadingElement|HTMLHtmlElement|HTMLKeygenElement|HTMLLIElement|HTMLLabelElement|HTMLLegendElement|HTMLLinkElement|HTMLMapElement|HTMLMarqueeElement|HTMLMenuElement|HTMLMenuItemElement|HTMLMetaElement|HTMLMeterElement|HTMLModElement|HTMLOListElement|HTMLOptGroupElement|HTMLOptionElement|HTMLOutputElement|HTMLParagraphElement|HTMLParamElement|HTMLPictureElement|HTMLPreElement|HTMLProgressElement|HTMLQuoteElement|HTMLShadowElement|HTMLSpanElement|HTMLStyleElement|HTMLTableCaptionElement|HTMLTableCellElement|HTMLTableColElement|HTMLTableDataCellElement|HTMLTableElement|HTMLTableHeaderCellElement|HTMLTableRowElement|HTMLTableSectionElement|HTMLTemplateElement|HTMLTextAreaElement|HTMLTitleElement|HTMLUListElement|HTMLUnknownElement|PluginPlaceholderElement;HTMLElement"},
hu:{"^":"v;",
i:function(a){return String(a)},
$isf:1,
"%":"HTMLAnchorElement"},
hw:{"^":"v;",
i:function(a){return String(a)},
$isf:1,
"%":"HTMLAreaElement"},
hx:{"^":"v;",
gaL:function(a){return H.i(new W.bm(a,"load",!1),[H.F(C.e,0)])},
$isf:1,
"%":"HTMLBodyElement"},
bQ:{"^":"v;T:height},U:width}",
aW:function(a,b,c){return a.getContext(b,P.fX(c,null))},
c1:function(a,b,c,d,e,f,g){var z,y
z=P.a_(["alpha",!0,"depth",!0,"stencil",!1,"antialias",!0,"premultipliedAlpha",!0,"preserveDrawingBuffer",!1])
y=this.aW(a,"webgl",z)
return y==null?this.aW(a,"experimental-webgl",z):y},
c0:function(a){return this.c1(a,!0,!0,!0,!0,!1,!1)},
$isbQ:1,
"%":"HTMLCanvasElement"},
hz:{"^":"f;",
i:function(a){return String(a)},
"%":"DOMException"},
bV:{"^":"eo;",
i:function(a){return a.localName},
gaL:function(a){return H.i(new W.bm(a,"load",!1),[H.F(C.e,0)])},
$isf:1,
"%":";Element"},
hA:{"^":"v;T:height},G:src},U:width}","%":"HTMLEmbedElement"},
hB:{"^":"X;R:error=","%":"ErrorEvent"},
X:{"^":"f;",$isX:1,$isa:1,"%":"AnimationEvent|AnimationPlayerEvent|ApplicationCacheErrorEvent|AudioProcessingEvent|AutocompleteErrorEvent|BeforeInstallPromptEvent|BeforeUnloadEvent|ClipboardEvent|CloseEvent|CompositionEvent|CrossOriginConnectEvent|CustomEvent|DefaultSessionStartEvent|DeviceLightEvent|DeviceMotionEvent|DeviceOrientationEvent|DragEvent|ExtendableEvent|FetchEvent|FocusEvent|FontFaceSetLoadEvent|GamepadEvent|GeofencingEvent|HashChangeEvent|IDBVersionChangeEvent|KeyboardEvent|MIDIConnectionEvent|MIDIMessageEvent|MediaEncryptedEvent|MediaKeyEvent|MediaKeyMessageEvent|MediaQueryListEvent|MediaStreamEvent|MediaStreamTrackEvent|MessageEvent|MouseEvent|NotificationEvent|OfflineAudioCompletionEvent|PageTransitionEvent|PeriodicSyncEvent|PointerEvent|PopStateEvent|ProgressEvent|PromiseRejectionEvent|PushEvent|RTCDTMFToneChangeEvent|RTCDataChannelEvent|RTCIceCandidateEvent|RTCPeerConnectionIceEvent|RelatedEvent|ResourceProgressEvent|SVGZoomEvent|SecurityPolicyViolationEvent|ServicePortConnectEvent|ServiceWorkerMessageEvent|SpeechRecognitionEvent|SpeechSynthesisEvent|StorageEvent|SyncEvent|TextEvent|TouchEvent|TrackEvent|TransitionEvent|UIEvent|WebGLContextEvent|WebKitTransitionEvent|WheelEvent|XMLHttpRequestProgressEvent;Event|InputEvent"},
bY:{"^":"f;",
cu:function(a,b,c,d){return a.addEventListener(b,H.a5(c,1),!1)},
cN:function(a,b,c,d){return a.removeEventListener(b,H.a5(c,1),!1)},
"%":"MediaStream;EventTarget"},
hT:{"^":"v;j:length=","%":"HTMLFormElement"},
hV:{"^":"v;T:height},G:src},U:width}","%":"HTMLIFrameElement"},
c0:{"^":"v;T:height},G:src},U:width}",$isc0:1,"%":"HTMLImageElement"},
hX:{"^":"v;T:height},G:src},U:width}",$isf:1,"%":"HTMLInputElement"},
em:{"^":"v;R:error=,G:src}","%":"HTMLAudioElement;HTMLMediaElement"},
ia:{"^":"f;",$isf:1,"%":"Navigator"},
eo:{"^":"bY;",
i:function(a){var z=a.nodeValue
return z==null?this.ck(a):z},
"%":"Document|HTMLDocument;Node"},
ic:{"^":"v;T:height},U:width}","%":"HTMLObjectElement"},
ig:{"^":"v;G:src}","%":"HTMLScriptElement"},
ii:{"^":"v;j:length=","%":"HTMLSelectElement"},
ij:{"^":"v;G:src}","%":"HTMLSourceElement"},
ik:{"^":"X;R:error=","%":"SpeechRecognitionError"},
ip:{"^":"v;G:src}","%":"HTMLTrackElement"},
cD:{"^":"em;T:height},U:width}",$iscD:1,"%":"HTMLVideoElement"},
eV:{"^":"bY;",
gbu:function(a){var z=H.i(new P.fE(H.i(new P.S(0,$.m,null),[P.a6])),[P.a6])
this.cD(a)
this.cO(a,W.au(new W.eW(z)))
return z.a},
cO:function(a,b){return a.requestAnimationFrame(H.a5(b,1))},
cD:function(a){if(!!(a.requestAnimationFrame&&a.cancelAnimationFrame))return;(function(b){var z=['ms','moz','webkit','o']
for(var y=0;y<z.length&&!b.requestAnimationFrame;++y){b.requestAnimationFrame=b[z[y]+'RequestAnimationFrame']
b.cancelAnimationFrame=b[z[y]+'CancelAnimationFrame']||b[z[y]+'CancelRequestAnimationFrame']}if(b.requestAnimationFrame&&b.cancelAnimationFrame)return
b.requestAnimationFrame=function(c){return window.setTimeout(function(){c(Date.now())},16)}
b.cancelAnimationFrame=function(c){clearTimeout(c)}})(a)},
$isf:1,
"%":"DOMWindow|Window"},
eW:{"^":"d:2;a",
$1:function(a){var z=this.a.a
if(z.a!==0)H.r(new P.aK("Future already completed"))
z.a_(a)}},
iy:{"^":"v;",$isf:1,"%":"HTMLFrameSetElement"},
bX:{"^":"a;a"},
cH:{"^":"R;a,b,c",
X:function(a,b,c,d){var z=new W.aO(0,this.a,this.b,W.au(a),!1)
z.$builtinTypeInfo=this.$builtinTypeInfo
z.a4()
return z},
bG:function(a,b,c){return this.X(a,null,b,c)}},
bm:{"^":"cH;a,b,c"},
aO:{"^":"eE;a,b,c,d,e",
aJ:function(){if(this.b==null)return
this.bq()
this.b=null
this.d=null
return},
aM:function(a,b){if(this.b==null)return;++this.a
this.bq()},
bJ:function(a){return this.aM(a,null)},
bL:function(){if(this.b==null||this.a<=0)return;--this.a
this.a4()},
a4:function(){var z,y,x
z=this.d
y=z!=null
if(y&&this.a<=0){x=this.b
x.toString
if(y)J.dh(x,this.c,z,!1)}},
bq:function(){var z,y,x
z=this.d
y=z!=null
if(y){x=this.b
x.toString
if(y)J.di(x,this.c,z,!1)}}}}],["","",,P,{"^":"",
fX:function(a,b){var z={}
a.w(0,new P.fY(z))
return z},
fY:{"^":"d:13;a",
$2:function(a,b){this.a[a]=b}}}],["","",,P,{"^":""}],["","",,P,{"^":"",fo:{"^":"a;",
bI:function(a){if(a<=0||a>4294967296)throw H.c(P.es("max must be in range 0 < max \u2264 2^32, was "+a))
return Math.random()*a>>>0}}}],["","",,P,{"^":"",ht:{"^":"am;",$isf:1,"%":"SVGAElement"},hv:{"^":"o;",$isf:1,"%":"SVGAnimateElement|SVGAnimateMotionElement|SVGAnimateTransformElement|SVGAnimationElement|SVGSetElement"},hC:{"^":"o;",$isf:1,"%":"SVGFEBlendElement"},hD:{"^":"o;",$isf:1,"%":"SVGFEColorMatrixElement"},hE:{"^":"o;",$isf:1,"%":"SVGFEComponentTransferElement"},hF:{"^":"o;",$isf:1,"%":"SVGFECompositeElement"},hG:{"^":"o;",$isf:1,"%":"SVGFEConvolveMatrixElement"},hH:{"^":"o;",$isf:1,"%":"SVGFEDiffuseLightingElement"},hI:{"^":"o;",$isf:1,"%":"SVGFEDisplacementMapElement"},hJ:{"^":"o;",$isf:1,"%":"SVGFEFloodElement"},hK:{"^":"o;",$isf:1,"%":"SVGFEGaussianBlurElement"},hL:{"^":"o;",$isf:1,"%":"SVGFEImageElement"},hM:{"^":"o;",$isf:1,"%":"SVGFEMergeElement"},hN:{"^":"o;",$isf:1,"%":"SVGFEMorphologyElement"},hO:{"^":"o;",$isf:1,"%":"SVGFEOffsetElement"},hP:{"^":"o;",$isf:1,"%":"SVGFESpecularLightingElement"},hQ:{"^":"o;",$isf:1,"%":"SVGFETileElement"},hR:{"^":"o;",$isf:1,"%":"SVGFETurbulenceElement"},hS:{"^":"o;",$isf:1,"%":"SVGFilterElement"},am:{"^":"o;",$isf:1,"%":"SVGCircleElement|SVGClipPathElement|SVGDefsElement|SVGEllipseElement|SVGForeignObjectElement|SVGGElement|SVGGeometryElement|SVGLineElement|SVGPathElement|SVGPolygonElement|SVGPolylineElement|SVGRectElement|SVGSwitchElement;SVGGraphicsElement"},hW:{"^":"am;",$isf:1,"%":"SVGImageElement"},i_:{"^":"o;",$isf:1,"%":"SVGMarkerElement"},i0:{"^":"o;",$isf:1,"%":"SVGMaskElement"},id:{"^":"o;",$isf:1,"%":"SVGPatternElement"},ih:{"^":"o;",$isf:1,"%":"SVGScriptElement"},o:{"^":"bV;",
gaL:function(a){return H.i(new W.bm(a,"load",!1),[H.F(C.e,0)])},
$isf:1,
"%":"SVGComponentTransferFunctionElement|SVGDescElement|SVGDiscardElement|SVGFEDistantLightElement|SVGFEFuncAElement|SVGFEFuncBElement|SVGFEFuncGElement|SVGFEFuncRElement|SVGFEMergeNodeElement|SVGFEPointLightElement|SVGFESpotLightElement|SVGMetadataElement|SVGStopElement|SVGStyleElement|SVGTitleElement;SVGElement"},il:{"^":"am;",$isf:1,"%":"SVGSVGElement"},im:{"^":"o;",$isf:1,"%":"SVGSymbolElement"},eN:{"^":"am;","%":"SVGTSpanElement|SVGTextElement|SVGTextPositioningElement;SVGTextContentElement"},io:{"^":"eN;",$isf:1,"%":"SVGTextPathElement"},iq:{"^":"am;",$isf:1,"%":"SVGUseElement"},ir:{"^":"o;",$isf:1,"%":"SVGViewElement"},ix:{"^":"o;",$isf:1,"%":"SVGGradientElement|SVGLinearGradientElement|SVGRadialGradientElement"},iz:{"^":"o;",$isf:1,"%":"SVGCursorElement"},iA:{"^":"o;",$isf:1,"%":"SVGFEDropShadowElement"},iB:{"^":"o;",$isf:1,"%":"SVGMPathElement"}}],["","",,P,{"^":""}],["","",,P,{"^":"",ie:{"^":"f;",
cS:function(a,b){return a.activeTexture(b)},
cU:function(a,b,c){return a.attachShader(b,c)},
cV:function(a,b,c){return a.bindBuffer(b,c)},
cW:function(a,b,c){return a.bindTexture(b,c)},
cY:function(a,b,c,d){return a.bufferData(b,c,d)},
d_:function(a,b){return a.clear(b)},
d0:function(a,b,c,d,e){return a.clearColor(b,c,d,e)},
d1:function(a,b){return a.compileShader(b)},
d4:function(a){return a.createBuffer()},
d5:function(a){return a.createProgram()},
d6:function(a,b){return a.createShader(b)},
d7:function(a){return a.createTexture()},
de:function(a,b,c,d,e){return a.drawElements(b,c,d,e)},
df:function(a,b){return a.enable(b)},
dg:function(a,b){return a.enableVertexAttribArray(b)},
bZ:function(a,b){return a.generateMipmap(b)},
c_:function(a,b,c){return a.getAttribLocation(b,c)},
c2:function(a,b){return a.getProgramInfoLog(b)},
c3:function(a,b,c){return a.getProgramParameter(b,c)},
c4:function(a,b){return a.getShaderInfoLog(b)},
c5:function(a,b,c){return a.getShaderParameter(b,c)},
c6:function(a,b,c){return a.getUniformLocation(b,c)},
dA:function(a,b){return a.linkProgram(b)},
cf:function(a,b,c){return a.shaderSource(b,c)},
dJ:function(a,b,c,d,e,f,g,h,i,j){var z,y
z=J.n(g)
if(!!z.$isc0)y=!0
else y=!1
if(y){a.texImage2D(b,c,d,e,f,g)
return}if(!!z.$isbQ)y=!0
else y=!1
if(y){a.texImage2D(b,c,d,e,f,g)
return}if(!!z.$iscD)z=!0
else z=!1
if(z){a.texImage2D(b,c,d,e,f,g)
return}throw H.c(P.b4("Incorrect number or type of arguments"))},
dI:function(a,b,c,d,e,f,g){return this.dJ(a,b,c,d,e,f,g,null,null,null)},
dK:function(a,b,c,d){return a.texParameteri(b,c,d)},
dL:function(a,b,c){return a.uniform1f(b,c)},
dM:function(a,b,c,d,e){return a.uniform3f(b,c,d,e)},
dN:function(a,b,c,d){return a.uniformMatrix4fv(b,!1,d)},
dO:function(a,b){return a.useProgram(b)},
dP:function(a,b){return a.validateProgram(b)},
dQ:function(a,b,c,d,e,f,g){return a.vertexAttribPointer(b,c,d,!1,f,g)},
dR:function(a,b,c,d,e){return a.viewport(b,c,d,e)},
"%":"WebGLRenderingContext"}}],["","",,P,{"^":""}],["","",,A,{"^":"",
bA:function(a){var z,y
z=C.A.di(a,0,new A.h1())
if(typeof z!=="number")return H.x(z)
y=536870911&z+((67108863&z)<<3>>>0)
y=(y^y>>>11)>>>0
return 536870911&y+((16383&y)<<15>>>0)},
h1:{"^":"d:4;",
$2:function(a,b){var z,y
z=J.a7(a,b&0x1FFFFFFF)
if(typeof z!=="number")return H.x(z)
y=536870911&z
y=536870911&y+((524287&y)<<10>>>0)
return y^y>>>6}}}],["","",,E,{"^":"",i1:{"^":"a;"},O:{"^":"a;bc:a<",
m:function(a){var z,y
z=a.a
y=this.a
y[15]=z[15]
y[14]=z[14]
y[13]=z[13]
y[12]=z[12]
y[11]=z[11]
y[10]=z[10]
y[9]=z[9]
y[8]=z[8]
y[7]=z[7]
y[6]=z[6]
y[5]=z[5]
y[4]=z[4]
y[3]=z[3]
y[2]=z[2]
y[1]=z[1]
y[0]=z[0]},
i:function(a){return"[0] "+this.ad(0).i(0)+"\n[1] "+this.ad(1).i(0)+"\n[2] "+this.ad(2).i(0)+"\n[3] "+this.ad(3).i(0)+"\n"},
h:function(a,b){var z=this.a
if(b>>>0!==b||b>=16)return H.e(z,b)
return z[b]},
u:function(a,b,c){var z=this.a
if(b>>>0!==b||b>=16)return H.e(z,b)
z[b]=c},
l:function(a,b){var z,y,x
if(b==null)return!1
if(b instanceof E.O){z=this.a
y=z[0]
x=b.a
z=y===x[0]&&z[1]===x[1]&&z[2]===x[2]&&z[3]===x[3]&&z[4]===x[4]&&z[5]===x[5]&&z[6]===x[6]&&z[7]===x[7]&&z[8]===x[8]&&z[9]===x[9]&&z[10]===x[10]&&z[11]===x[11]&&z[12]===x[12]&&z[13]===x[13]&&z[14]===x[14]&&z[15]===x[15]}else z=!1
return z},
gt:function(a){return A.bA(this.a)},
ad:function(a){var z,y,x
z=new Float64Array(H.l(4))
y=this.a
if(a>=16)return H.e(y,a)
z[0]=y[a]
x=4+a
if(x>=16)return H.e(y,x)
z[1]=y[x]
x=8+a
if(x>=16)return H.e(y,x)
z[2]=y[x]
x=12+a
if(x>=16)return H.e(y,x)
z[3]=y[x]
return new E.ae(z)},
L:function(a,b){var z=new E.O(new Float64Array(H.l(16)))
z.m(this)
z.aX(0,b,null,null)
return z},
k:function(a,b){var z,y,x
z=new Float64Array(H.l(16))
y=new E.O(z)
y.m(this)
x=b.gbc()
z[0]=C.a.k(z[0],x.h(0,0))
z[1]=C.a.k(z[1],x.h(0,1))
z[2]=C.a.k(z[2],x.h(0,2))
z[3]=C.a.k(z[3],x.h(0,3))
z[4]=C.a.k(z[4],x.h(0,4))
z[5]=C.a.k(z[5],x.h(0,5))
z[6]=C.a.k(z[6],x.h(0,6))
z[7]=C.a.k(z[7],x.h(0,7))
z[8]=C.a.k(z[8],x.h(0,8))
z[9]=C.a.k(z[9],x.h(0,9))
z[10]=C.a.k(z[10],x.h(0,10))
z[11]=C.a.k(z[11],x.h(0,11))
z[12]=C.a.k(z[12],x.h(0,12))
z[13]=C.a.k(z[13],x.h(0,13))
z[14]=C.a.k(z[14],x.h(0,14))
z[15]=C.a.k(z[15],x.h(0,15))
return y},
Z:function(a,b){var z,y,x
z=new Float64Array(H.l(16))
y=new E.O(z)
y.m(this)
x=b.gbc()
z[0]=z[0]-x[0]
z[1]=z[1]-x[1]
z[2]=z[2]-x[2]
z[3]=z[3]-x[3]
z[4]=z[4]-x[4]
z[5]=z[5]-x[5]
z[6]=z[6]-x[6]
z[7]=z[7]-x[7]
z[8]=z[8]-x[8]
z[9]=z[9]-x[9]
z[10]=z[10]-x[10]
z[11]=z[11]-x[11]
z[12]=z[12]-x[12]
z[13]=z[13]-x[13]
z[14]=z[14]-x[14]
z[15]=z[15]-x[15]
return y},
aU:function(a,b,c,d){var z,y,x,w,v,u,t,s,r,q,p,o,n,m,l,k,j,i,h,g
z=J.n(b)
if(!!z.$isq||!1){y=z.gbV(b)
x=z.gbW(b)
w=z.gbX(b)}else{w=d
x=c
y=b}z=this.a
v=z[0]
if(typeof y!=="number")return H.x(y)
u=z[4]
t=z[8]
s=z[12]
r=z[1]
q=z[5]
p=z[9]
o=z[13]
n=z[2]
m=z[6]
l=z[10]
k=z[14]
j=z[3]
i=z[7]
h=z[11]
g=z[15]
z[12]=v*y+u*x+t*w+s
z[13]=r*y+q*x+p*w+o
z[14]=n*y+m*x+l*w+k
z[15]=j*y+i*x+h*w+g},
bR:function(a,b){return this.aU(a,b,0,0)},
aX:function(a,b,c,d){var z,y,x,w,v
z=J.n(b)
if(!!z.$isq||!1){y=z.gbV(b)
x=z.gbW(b)
w=z.gbX(b)}else{w=b
x=w
y=x}z=this.a
v=z[0]
if(typeof y!=="number")return H.x(y)
z[0]=v*y
z[1]=z[1]*y
z[2]=z[2]*y
z[3]=z[3]*y
v=z[4]
if(typeof x!=="number")return H.x(x)
z[4]=v*x
z[5]=z[5]*x
z[6]=z[6]*x
z[7]=z[7]*x
v=z[8]
if(typeof w!=="number")return H.x(w)
z[8]=v*w
z[9]=z[9]*w
z[10]=z[10]*w
z[11]=z[11]*w
z[12]=z[12]
z[13]=z[13]
z[14]=z[14]
z[15]=z[15]},
F:function(a,b){return this.aX(a,b,null,null)},
aY:function(){var z=this.a
z[0]=1
z[1]=0
z[2]=0
z[3]=0
z[4]=0
z[5]=1
z[6]=0
z[7]=0
z[8]=0
z[9]=0
z[10]=1
z[11]=0
z[12]=0
z[13]=0
z[14]=0
z[15]=1}},q:{"^":"a;br:a<",
an:function(a,b,c){var z=this.a
z[0]=a
z[1]=b
z[2]=c},
m:function(a){var z,y
z=a.a
y=this.a
y[0]=z[0]
y[1]=z[1]
y[2]=z[2]},
N:function(a){var z=this.a
z[2]=a
z[1]=a
z[0]=a},
i:function(a){var z=this.a
return"["+H.b(z[0])+","+H.b(z[1])+","+H.b(z[2])+"]"},
l:function(a,b){var z,y,x
if(b==null)return!1
if(b instanceof E.q){z=this.a
y=z[0]
x=b.a
z=y===x[0]&&z[1]===x[1]&&z[2]===x[2]}else z=!1
return z},
gt:function(a){return A.bA(this.a)},
Z:function(a,b){var z=new E.q(new Float64Array(H.l(3)))
z.m(this)
z.ao(b)
return z},
k:function(a,b){var z=new E.q(new Float64Array(H.l(3)))
z.m(this)
z.v(0,b)
return z},
ac:function(a,b){var z=new E.q(new Float64Array(H.l(3)))
z.m(this)
z.F(0,1/b)
return z},
L:function(a,b){var z=new E.q(new Float64Array(H.l(3)))
z.m(this)
z.F(0,b)
return z},
h:function(a,b){var z=this.a
if(b>>>0!==b||b>=3)return H.e(z,b)
return z[b]},
u:function(a,b,c){var z=this.a
if(b>>>0!==b||b>=3)return H.e(z,b)
z[b]=c},
gj:function(a){return Math.sqrt(H.z(this.gbF()))},
gbF:function(){var z,y,x
z=this.a
y=z[0]
x=z[1]
z=z[2]
return y*y+x*x+z*z},
ak:function(){var z,y,x
z=Math.sqrt(H.z(this.gbF()))
if(z===0)return 0
y=1/z
x=this.a
x[0]=x[0]*y
x[1]=x[1]*y
x[2]=x[2]*y
return z},
ai:function(a){var z,y
z=a.a
y=this.a
return y[0]*z[0]+y[1]*z[1]+y[2]*z[2]},
bz:function(a){var z,y,x,w,v,u,t,s
z=this.a
y=z[0]
x=z[1]
w=z[2]
v=a.a
u=v[0]
t=v[1]
s=v[2]
z=new E.q(new Float64Array(H.l(3)))
z.an(x*s-w*t,w*u-y*s,y*t-x*u)
return z},
v:function(a,b){var z,y
z=b.gbr()
y=this.a
y[0]=y[0]+z[0]
y[1]=y[1]+z[1]
y[2]=y[2]+z[2]},
ao:function(a){var z,y
z=a.gbr()
y=this.a
y[0]=y[0]-z[0]
y[1]=y[1]-z[1]
y[2]=y[2]-z[2]},
F:function(a,b){var z=this.a
z[2]=z[2]*b
z[1]=z[1]*b
z[0]=z[0]*b},
gbV:function(a){return this.a[0]},
gbW:function(a){return this.a[1]},
gbX:function(a){return this.a[2]}},ae:{"^":"a;bs:a<",
m:function(a){var z,y
z=a.a
y=this.a
y[3]=z[3]
y[2]=z[2]
y[1]=z[1]
y[0]=z[0]},
i:function(a){var z=this.a
return H.b(z[0])+","+H.b(z[1])+","+H.b(z[2])+","+H.b(z[3])},
l:function(a,b){var z,y,x
if(b==null)return!1
if(b instanceof E.ae){z=this.a
y=z[0]
x=b.a
z=y===x[0]&&z[1]===x[1]&&z[2]===x[2]&&z[3]===x[3]}else z=!1
return z},
gt:function(a){return A.bA(this.a)},
Z:function(a,b){var z,y,x
z=new Float64Array(H.l(4))
y=new E.ae(z)
y.m(this)
x=b.gbs()
z[0]=z[0]-x[0]
z[1]=z[1]-x[1]
z[2]=z[2]-x[2]
z[3]=z[3]-x[3]
return y},
k:function(a,b){var z,y,x
z=new Float64Array(H.l(4))
y=new E.ae(z)
y.m(this)
x=b.gbs()
z[0]=C.a.k(z[0],x.h(0,0))
z[1]=C.a.k(z[1],x.h(0,1))
z[2]=C.a.k(z[2],x.h(0,2))
z[3]=C.a.k(z[3],x.h(0,3))
return y},
ac:function(a,b){var z=new E.ae(new Float64Array(H.l(4)))
z.m(this)
z.F(0,1/b)
return z},
L:function(a,b){var z=new E.ae(new Float64Array(H.l(4)))
z.m(this)
z.F(0,b)
return z},
h:function(a,b){var z=this.a
if(b>>>0!==b||b>=4)return H.e(z,b)
return z[b]},
u:function(a,b,c){var z=this.a
if(b>>>0!==b||b>=4)return H.e(z,b)
z[b]=c},
gj:function(a){var z,y,x,w
z=this.a
y=z[0]
x=z[1]
w=z[2]
z=z[3]
return Math.sqrt(H.z(y*y+x*x+w*w+z*z))},
F:function(a,b){var z=this.a
z[0]=z[0]*b
z[1]=z[1]*b
z[2]=z[2]*b
z[3]=z[3]*b}}}],["","",,F,{"^":"",
iG:[function(){var z=document.querySelector("#webgl")
$.aS=z
if(z==null)H.r(P.E("Failed to find canvas"))
z=document.querySelector("#title")
$.dc=z
if(z==null)H.r(P.E("Failed to find title"))
z=H.i(new W.cH(window,"resize",!1),[H.F(C.p,0)])
H.i(new W.aO(0,z.a,z.b,W.au(F.hj()),!1),[H.F(z,0)]).a4()
z=J.dx($.aS)
$.h=z
if(z==null)H.r(P.E("Failed to initializte OpenGL"))
J.dm(z,0,0,0,1)
J.du($.h,2929)
J.dj($.h,33984)
F.h2()
F.d7()
C.l.gbu(window).aR(F.d3())},"$0","d4",0,0,1],
h2:function(){var z,y,x,w
z=new F.eC(!1,null,null,null)
z.b=J.dp($.h)
z.c=z.by(0,"precision highp float;\r\n\r\nattribute vec3 pos;\r\nattribute vec3 norm;\r\nattribute vec2 coord;\r\n\r\nuniform mat4 model;\r\nuniform mat4 projection;\r\nuniform mat4 view;\r\n\r\nvarying vec3 normal;\r\nvarying vec3 fragPos;\r\nvarying vec2 texCoord;\r\n\r\nhighp mat4 transpose(in highp mat4 inMatrix);\r\nhighp mat4 inverse(in highp mat4 inMatrix);\r\n\r\nvoid main(void) {\r\n\tgl_Position = projection * view * model * vec4(pos, 1.0);\r\n\r\n\tfragPos = vec3(model * vec4(pos, 1.0));\r\n\tnormal = mat3(transpose(inverse(model))) * norm;\r\n\ttexCoord = coord;\r\n}\r\n\r\nhighp mat4 transpose(in highp mat4 inMatrix) {\r\n    highp vec4 i0 = inMatrix[0];\r\n    highp vec4 i1 = inMatrix[1];\r\n    highp vec4 i2 = inMatrix[2];\r\n    highp vec4 i3 = inMatrix[3];\r\n\r\n    highp mat4 outMatrix = mat4(\r\n                 vec4(i0.x, i1.x, i2.x, i3.x),\r\n                 vec4(i0.y, i1.y, i2.y, i3.y),\r\n                 vec4(i0.z, i1.z, i2.z, i3.z),\r\n                 vec4(i0.w, i1.w, i2.w, i3.w)\r\n                 );\r\n\r\n    return outMatrix;\r\n}\r\n\r\nhighp mat4 inverse(in highp mat4 inMatrix) {\r\n    highp vec4 i0 = inMatrix[0];\r\n    highp vec4 i1 = inMatrix[1];\r\n    highp vec4 i2 = inMatrix[2];\r\n    highp vec4 i3 = inMatrix[3];\r\n\r\n    highp mat4 outMatrix = mat4(\r\n                     vec4(i0.x, i1.x, i2.x, i3.x),\r\n                     vec4(i0.y, i1.y, i2.y, i3.y),\r\n                     vec4(i0.z, i1.z, i2.z, i3.z),\r\n                     vec4(i0.w, i1.w, i2.w, i3.w)\r\n                     );\r\n\r\n    return outMatrix;\r\n}\r\n        ",35633)
z.d=z.by(0,"precision highp float;\r\n\r\nvarying vec3 normal;\r\nvarying vec3 fragPos;\r\nvarying vec2 texCoord;\r\n\r\nstruct Material {\r\n\tvec3 color;\r\n\tvec3 ambient;\r\n\tvec3 specular;\r\n\tfloat shininess;\r\n};\r\n\r\nstruct Light {\r\n\tvec3 ambient;\r\n\tvec3 diffuse;\r\n\tvec3 specular;\r\n\tvec3 pos;\r\n\r\n\tfloat constant;\r\n\tfloat linear;\r\n\tfloat quadratic;\r\n};\r\n\r\nuniform sampler2D texture;\r\nuniform Material mat;\r\nuniform Light light;\r\nuniform vec3 viewPos;\r\nuniform bool useTexture;\r\nuniform bool useFlatShading;\r\n\r\nvoid main(void) {\r\n    if (useTexture) {\r\n        vec4 tex = texture2D(texture, texCoord);\r\n        if (tex.w < 1.0)\r\n            discard;\r\n\r\n        gl_FragColor = tex;\r\n    } else {\r\n        gl_FragColor = vec4(mat.color.x, mat.color.y, mat.color.z, 1.0);\r\n    }\r\n}\r\n        ",35632)
J.dA($.h,z.b)
if(J.bJ($.h,z.b,35714)!==!0)H.r(P.E(J.bI($.h,z.b)))
J.dJ($.h,z.b)
if(J.bJ($.h,z.b,35715)!==!0)H.r(P.E(J.bI($.h,z.b)))
$.D=z
z=new F.dO(null,null,null,null,null,null,45,null,null,null,null,null,null,null,null,null,null,null,null,null,null)
z.b0(0,0,0,null)
y=new E.q(new Float64Array(H.l(3)))
y.an(0,1,0)
z.db=y
y=new E.q(new Float64Array(H.l(3)))
y.an(0,0,0)
z.dx=y
z.f=!1
$.aR=z
$.aX=H.i([],[F.b7])
x=W.c1(null,"res/tex.png",null)
z=J.bH(x)
H.i(new W.aO(0,z.a,z.b,W.au(new F.h3(x)),!1),[H.F(z,0)]).a4()
w=W.c1(null,"res/grass.png",null)
z=J.bH(w)
H.i(new W.aO(0,z.a,z.b,W.au(new F.h4(w)),!1),[H.F(z,0)]).a4()},
iF:[function(a){var z,y,x,w,v,u,t,s,r,q,p,o,n,m,l,k,j,i,h,g
z=J.bx(a)
y=J.de(z.Z(a,$.d2),1000)
$.d2=a
x=$.bw
if(typeof y!=="number")return H.x(y)
x+=y
$.bw=x
if(x>1){w=C.r.bM(1/y)
$.dc.textContent="WebPage "+w+" fps"
$.bw=0}x=$.aR
v=x.a.a
v[0]=v[0]+Math.sin(H.z(z.L(a,0.001)))*10*y
v=x.a.a
v[2]=v[2]+Math.cos(H.z(z.L(a,0.001)))*10*y
z=x.a
v=x.b
u=new E.q(new Float64Array(H.l(3)))
u.m(z)
u.v(0,v)
v=x.dx
z=x.db
t=new Float64Array(H.l(16))
s=new E.O(t)
r=new Float64Array(H.l(3))
q=new E.q(r)
q.m(u)
q.ao(v)
q.ak()
p=z.bz(q)
p.ak()
o=q.bz(p)
o.ak()
z=p.ai(u)
v=o.ai(u)
u=q.ai(u)
n=p.a
m=n[0]
l=o.a
k=l[0]
j=r[0]
i=n[1]
h=l[1]
g=r[1]
n=n[2]
l=l[2]
r=r[2]
t[15]=1
t[14]=-u
t[13]=-v
t[12]=-z
t[11]=0
t[10]=r
t[9]=l
t[8]=n
t[7]=0
t[6]=g
t[5]=h
t[4]=i
t[3]=0
t[2]=j
t[1]=k
t[0]=m
x.dy=s
x.E($.D,"view",s)
m=$.D
t=x.a
k=x.b
j=new E.q(new Float64Array(H.l(3)))
j.m(t)
j.v(0,k)
x.E(m,"viewPos",j)
F.hn()
C.l.gbu(window).aR(F.d3())},"$1","d3",2,0,14],
hn:function(){J.dl($.h,16640)
var z=$.aX;(z&&C.d).w(z,new F.ho())},
d7:function(){var z,y,x,w,v,u,t,s,r,q
J.dE($.aS,window.innerWidth)
J.dC($.aS,window.innerHeight)
J.dK($.h,0,0,window.innerWidth,window.innerHeight)
z=$.aR
y=window.innerWidth
x=window.innerHeight
z.fx=y
z.fy=x
w=z.go
y.toString
x.toString
if(typeof y!=="number")return y.ac()
if(typeof x!=="number")return H.x(x)
v=new Float64Array(H.l(16))
u=new E.O(v)
t=Math.tan(H.z(w*0.017453292519943295*0.5))*0.1
s=t*(y/x)
x=-s
y=-t
r=s-x
q=t-y
v[0]=0
v[1]=0
v[2]=0
v[3]=0
v[4]=0
v[5]=0
v[6]=0
v[7]=0
v[8]=0
v[9]=0
v[10]=0
v[11]=0
v[12]=0
v[13]=0
v[14]=0
v[15]=0
v[0]=0.2/r
v[5]=0.2/q
v[8]=(s+x)/r
v[9]=(t+y)/q
v[10]=-1.002002002002002
v[11]=-1
v[14]=-0.20020020020020018
z.fr=u
z.E($.D,"projection",u)},
iH:[function(a){F.d7()},"$1","hj",2,0,15],
dM:{"^":"a;",
bS:function(a,b,c,d,e){var z,y,x,w,v
z=J.dz($.h,a.b,b)
if(!a.a){J.dI($.h,a.b)
a.a=!0}y=J.n(c)
if(!!y.$isq){y=$.h
x=c.a
J.bM(y,z,x[0],x[1],x[2])}else if(typeof c==="boolean"){y=$.h
if(c)J.b2(y,z,1)
else J.b2(y,z,0)}else if(typeof c==="number"){y=d!=null&&e!=null
x=$.h
if(y)J.bM(x,z,c,d,e)
else J.b2(x,z,c)}else if(!!y.$isO){y=H.l(16)
w=new Float32Array(y)
x=c.a
v=x[15]
if(15>=y)return H.e(w,15)
w[15]=v
w[14]=x[14]
w[13]=x[13]
w[12]=x[12]
w[11]=x[11]
w[10]=x[10]
w[9]=x[9]
w[8]=x[8]
w[7]=x[7]
w[6]=x[6]
w[5]=x[5]
w[4]=x[4]
w[3]=x[3]
w[2]=x[2]
w[1]=x[1]
w[0]=x[0]
J.dH($.h,z,!1,w)}else throw H.c(P.E(C.i.k("Unimplemented uniform type ",c)))},
E:function(a,b,c){return this.bS(a,b,c,null,null)}},
dO:{"^":"b7;db,dx,dy,fr,fx,fy,go,a,b,c,d,e,f,r,x,y,z,Q,ch,cx,cy",
bA:function(){}},
b7:{"^":"dM;",
dF:function(){var z,y,x,w,v,u,t,s,r,q,p,o,n,m,l
z=this.ch
y=$.D
if(z!=null)this.E(y,"useTexture",!0)
else this.E(y,"useTexture",!1)
if(!this.x){z=$.aR
y=z.a
z=z.b
x=new E.q(new Float64Array(H.l(3)))
x.m(y)
x.v(0,z)
z=this.a
y=this.b
w=new E.q(new Float64Array(H.l(3)))
w.m(z)
w.v(0,y)
v=new E.q(new Float64Array(H.l(3)))
v.m(x)
v.ao(w)
v.ak()
w=new Float64Array(H.l(3))
u=new E.q(w)
u.N(0)
x=this.Q.a
w[0]=x[8]
w[1]=x[9]
w[2]=x[10]
t=Math.acos(H.z(v.ai(u)))
x=this.z.a
this.b_(x[0]*57.29577951308232,t*57.29577951308232,x[2]*57.29577951308232)}this.Q.aY()
this.Q.bR(0,this.a)
this.Q.bR(0,this.b)
z=this.Q
y=this.e.a
z.aU(0,0.5*y[0],0.5*y[1],0)
y=this.Q
z=this.z.a[0]
s=Math.cos(H.z(z))
r=Math.sin(H.z(z))
y=y.a
z=y[4]
x=y[8]
w=y[5]
q=y[9]
p=y[6]
o=y[10]
n=y[7]
m=y[11]
l=-r
y[4]=z*s+x*r
y[5]=w*s+q*r
y[6]=p*s+o*r
y[7]=n*s+m*r
y[8]=z*l+x*s
y[9]=w*l+q*s
y[10]=p*l+o*s
y[11]=n*l+m*s
m=this.Q
l=this.z.a[1]
s=Math.cos(H.z(l))
r=Math.sin(H.z(l))
m=m.a
l=m[0]
n=m[8]
y=-r
o=m[1]
p=m[9]
q=m[2]
w=m[10]
x=m[3]
z=m[11]
m[0]=l*s+n*y
m[1]=o*s+p*y
m[2]=q*s+w*y
m[3]=x*s+z*y
m[8]=l*r+n*s
m[9]=o*r+p*s
m[10]=q*r+w*s
m[11]=x*r+z*s
z=this.Q
x=this.z.a[2]
s=Math.cos(H.z(x))
r=Math.sin(H.z(x))
z=z.a
x=z[0]
m=z[4]
w=z[1]
q=z[5]
p=z[2]
o=z[6]
n=z[3]
l=z[7]
y=-r
z[0]=x*s+m*r
z[1]=w*s+q*r
z[2]=p*s+o*r
z[3]=n*s+l*r
z[4]=x*y+m*s
z[5]=w*y+q*s
z[6]=p*y+o*s
z[7]=n*y+l*s
l=this.Q
y=this.e.a
l.aU(0,-0.5*y[0],-0.5*y[1],0)
this.Q.F(0,this.e)
this.E($.D,"model",this.Q)
this.E($.D,"mat.color",this.c)
this.E($.D,"mat.ambient",this.d)
this.bS($.D,"mat.specular",0.5,0.5,0.5)
this.E($.D,"mat.shininess",32)
J.a8($.h,34962,this.cx)
J.a8($.h,34963,this.cy)
z=this.ch
if(z!=null)J.ax($.h,3553,z)
if(this.f)this.bA()
if(this.ch!=null)J.ax($.h,3553,null)
J.a8($.h,34962,null)
J.a8($.h,34963,null)},
b_:function(a,b,c){var z=this.z.a
z[0]=a*0.017453292519943295
z[1]=b*0.017453292519943295
z[2]=c*0.017453292519943295},
b0:function(a,b,c,d){var z,y
z=new E.q(new Float64Array(H.l(3)))
z.N(0)
this.c=z
z=new E.q(new Float64Array(H.l(3)))
z.N(0)
this.d=z
z=new E.q(new Float64Array(H.l(3)))
z.N(0)
this.a=z
z=new E.q(new Float64Array(H.l(3)))
z.N(0)
this.b=z
z=new E.q(new Float64Array(H.l(3)))
z.N(1)
this.e=z
z=new E.q(new Float64Array(H.l(3)))
z.N(0)
this.z=z
z=new E.O(new Float64Array(H.l(16)))
z.aY()
this.Q=z
this.f=!0
this.x=!1
this.r=!1
z=this.c
y=z.a
y[0]=1
y[1]=0
y[2]=0
this.d=z
y=new E.q(new Float64Array(H.l(3)))
y.m(z)
y.F(0,0.5)
this.d=y
z=this.a.a
z[0]=a
z[1]=b
z[2]=c
if(d!=null){z=J.dr($.h)
this.ch=z
J.ax($.h,3553,z)
J.bL($.h,3553,10241,9728)
J.bL($.h,3553,10240,9728)
J.dG($.h,3553,0,6408,6408,5121,d)
J.dw($.h,3553)
J.ax($.h,3553,null)}this.cx=J.bG($.h)
this.cy=J.bG($.h)}},
er:{"^":"b7;a,b,c,d,e,f,r,x,y,z,Q,ch,cx,cy",
bA:function(){J.ds($.h,4,6,5123,0)},
cp:function(a,b,c,d){var z,y,x,w,v
z=new Float32Array(H.cN([0.5,0.5,0,0,0,-1,1,1,-0.5,0.5,0,0,0,-1,1,0,-0.5,-0.5,0,0,0,-1,0,0,0.5,-0.5,0,0,0,-1,0,1]))
y=new Uint16Array(H.cN([0,1,2,0,2,3]))
x=J.b1($.h,$.D.b,"pos")
w=J.b1($.h,$.D.b,"norm")
v=J.b1($.h,$.D.b,"coord")
J.a8($.h,34962,this.cx)
J.bF($.h,34962,z,35044)
J.a8($.h,34963,this.cy)
J.bF($.h,34963,y,35044)
J.b3($.h,x,3,5126,!1,32,0)
J.b_($.h,x)
J.b3($.h,w,3,5126,!1,32,12)
J.b_($.h,w)
J.b3($.h,v,2,5126,!1,32,24)
J.b_($.h,v)},
n:{
ch:function(a,b,c,d){var z=new F.er(null,null,null,null,null,null,null,null,null,null,null,null,null,null)
z.b0(a,b,c,d)
z.cp(a,b,c,d)
return z}}},
eC:{"^":"a;a,b,c,d",
by:function(a,b,c){var z=J.dq($.h,c)
if(z==null)throw H.c(P.E(J.bK($.h,z)))
J.dF($.h,z,b)
J.dn($.h,z)
if(J.dy($.h,z,35713)!==!0)throw H.c(P.E(J.bK($.h,z)))
J.dk($.h,this.b,z)
return z}},
h3:{"^":"d:2;a",
$1:function(a){var z,y
z=F.ch(5,-12,5,this.a)
y=z.e.a
y[0]=20
y[1]=20
y[2]=1
z.b_(90,0,0)
z.x=!0
$.aX.push(z)}},
h4:{"^":"d:2;a",
$1:function(a){var z,y,x,w
for(z=this.a,y=0;y<30;++y){x=F.ch(C.f.bI(20)-10,-2,C.f.bI(20)-10,z)
w=x.z.a
w[0]=0
w[1]=0
w[2]=-1.5707963267948966
$.aX.push(x)}}},
ho:{"^":"d:2;",
$1:function(a){return a.dF()}}},1]]
setupProgram(dart,0)
J.n=function(a){if(typeof a=="number"){if(Math.floor(a)==a)return J.c6.prototype
return J.c5.prototype}if(typeof a=="string")return J.aC.prototype
if(a==null)return J.ea.prototype
if(typeof a=="boolean")return J.e9.prototype
if(a.constructor==Array)return J.an.prototype
if(typeof a!="object"){if(typeof a=="function")return J.ap.prototype
return a}if(a instanceof P.a)return a
return J.aU(a)}
J.L=function(a){if(typeof a=="string")return J.aC.prototype
if(a==null)return a
if(a.constructor==Array)return J.an.prototype
if(typeof a!="object"){if(typeof a=="function")return J.ap.prototype
return a}if(a instanceof P.a)return a
return J.aU(a)}
J.aw=function(a){if(a==null)return a
if(a.constructor==Array)return J.an.prototype
if(typeof a!="object"){if(typeof a=="function")return J.ap.prototype
return a}if(a instanceof P.a)return a
return J.aU(a)}
J.bx=function(a){if(typeof a=="number")return J.ao.prototype
if(a==null)return a
if(!(a instanceof P.a))return J.aM.prototype
return a}
J.h_=function(a){if(typeof a=="number")return J.ao.prototype
if(typeof a=="string")return J.aC.prototype
if(a==null)return a
if(!(a instanceof P.a))return J.aM.prototype
return a}
J.j=function(a){if(a==null)return a
if(typeof a!="object"){if(typeof a=="function")return J.ap.prototype
return a}if(a instanceof P.a)return a
return J.aU(a)}
J.a7=function(a,b){if(typeof a=="number"&&typeof b=="number")return a+b
return J.h_(a).k(a,b)}
J.de=function(a,b){if(typeof a=="number"&&typeof b=="number")return a/b
return J.bx(a).ac(a,b)}
J.T=function(a,b){if(a==null)return b==null
if(typeof a!="object")return b!=null&&a===b
return J.n(a).l(a,b)}
J.df=function(a,b){if(typeof a=="number"&&typeof b=="number")return a<b
return J.bx(a).al(a,b)}
J.dg=function(a,b){if(typeof b==="number")if(a.constructor==Array||typeof a=="string"||H.hh(a,a[init.dispatchPropertyName]))if(b>>>0===b&&b<a.length)return a[b]
return J.L(a).h(a,b)}
J.dh=function(a,b,c,d){return J.j(a).cu(a,b,c,d)}
J.di=function(a,b,c,d){return J.j(a).cN(a,b,c,d)}
J.dj=function(a,b){return J.j(a).cS(a,b)}
J.dk=function(a,b,c){return J.j(a).cU(a,b,c)}
J.a8=function(a,b,c){return J.j(a).cV(a,b,c)}
J.ax=function(a,b,c){return J.j(a).cW(a,b,c)}
J.bF=function(a,b,c,d){return J.j(a).cY(a,b,c,d)}
J.dl=function(a,b){return J.aw(a).d_(a,b)}
J.dm=function(a,b,c,d,e){return J.j(a).d0(a,b,c,d,e)}
J.dn=function(a,b){return J.j(a).d1(a,b)}
J.bG=function(a){return J.j(a).d4(a)}
J.dp=function(a){return J.j(a).d5(a)}
J.dq=function(a,b){return J.j(a).d6(a,b)}
J.dr=function(a){return J.j(a).d7(a)}
J.ds=function(a,b,c,d,e){return J.j(a).de(a,b,c,d,e)}
J.dt=function(a,b){return J.aw(a).J(a,b)}
J.du=function(a,b){return J.j(a).df(a,b)}
J.b_=function(a,b){return J.j(a).dg(a,b)}
J.dv=function(a,b){return J.aw(a).w(a,b)}
J.dw=function(a,b){return J.j(a).bZ(a,b)}
J.a9=function(a){return J.j(a).gR(a)}
J.ay=function(a){return J.n(a).gt(a)}
J.b0=function(a){return J.aw(a).gA(a)}
J.al=function(a){return J.L(a).gj(a)}
J.bH=function(a){return J.j(a).gaL(a)}
J.b1=function(a,b,c){return J.j(a).c_(a,b,c)}
J.dx=function(a){return J.j(a).c0(a)}
J.bI=function(a,b){return J.j(a).c2(a,b)}
J.bJ=function(a,b,c){return J.j(a).c3(a,b,c)}
J.bK=function(a,b){return J.j(a).c4(a,b)}
J.dy=function(a,b,c){return J.j(a).c5(a,b,c)}
J.dz=function(a,b,c){return J.j(a).c6(a,b,c)}
J.dA=function(a,b){return J.j(a).dA(a,b)}
J.dB=function(a,b){return J.aw(a).Y(a,b)}
J.dC=function(a,b){return J.j(a).sT(a,b)}
J.dD=function(a,b){return J.j(a).sG(a,b)}
J.dE=function(a,b){return J.j(a).sU(a,b)}
J.dF=function(a,b,c){return J.j(a).cf(a,b,c)}
J.dG=function(a,b,c,d,e,f,g){return J.j(a).dI(a,b,c,d,e,f,g)}
J.bL=function(a,b,c,d){return J.j(a).dK(a,b,c,d)}
J.U=function(a){return J.n(a).i(a)}
J.b2=function(a,b,c){return J.j(a).dL(a,b,c)}
J.bM=function(a,b,c,d,e){return J.j(a).dM(a,b,c,d,e)}
J.dH=function(a,b,c,d){return J.j(a).dN(a,b,c,d)}
J.dI=function(a,b){return J.j(a).dO(a,b)}
J.dJ=function(a,b){return J.j(a).dP(a,b)}
J.b3=function(a,b,c,d,e,f,g){return J.j(a).dQ(a,b,c,d,e,f,g)}
J.dK=function(a,b,c,d,e){return J.j(a).dR(a,b,c,d,e)}
var $=I.p
C.q=J.f.prototype
C.d=J.an.prototype
C.r=J.c5.prototype
C.c=J.c6.prototype
C.a=J.ao.prototype
C.i=J.aC.prototype
C.z=J.ap.prototype
C.A=H.en.prototype
C.B=J.eq.prototype
C.C=J.aM.prototype
C.l=W.eV.prototype
C.m=new H.bT()
C.n=new P.ep()
C.o=new P.f6()
C.f=new P.fo()
C.b=new P.fz()
C.h=new P.ab(0)
C.e=H.i(new W.bX("load"),[W.X])
C.p=H.i(new W.bX("resize"),[W.X])
C.t=function() {  function typeNameInChrome(o) {    var constructor = o.constructor;    if (constructor) {      var name = constructor.name;      if (name) return name;    }    var s = Object.prototype.toString.call(o);    return s.substring(8, s.length - 1);  }  function getUnknownTag(object, tag) {    if (/^HTML[A-Z].*Element$/.test(tag)) {      var name = Object.prototype.toString.call(object);      if (name == "[object Object]") return null;      return "HTMLElement";    }  }  function getUnknownTagGenericBrowser(object, tag) {    if (self.HTMLElement && object instanceof HTMLElement) return "HTMLElement";    return getUnknownTag(object, tag);  }  function prototypeForTag(tag) {    if (typeof window == "undefined") return null;    if (typeof window[tag] == "undefined") return null;    var constructor = window[tag];    if (typeof constructor != "function") return null;    return constructor.prototype;  }  function discriminator(tag) { return null; }  var isBrowser = typeof navigator == "object";  return {    getTag: typeNameInChrome,    getUnknownTag: isBrowser ? getUnknownTagGenericBrowser : getUnknownTag,    prototypeForTag: prototypeForTag,    discriminator: discriminator };}
C.j=function(hooks) { return hooks; }
C.u=function(hooks) {  if (typeof dartExperimentalFixupGetTag != "function") return hooks;  hooks.getTag = dartExperimentalFixupGetTag(hooks.getTag);}
C.v=function(hooks) {  var getTag = hooks.getTag;  var prototypeForTag = hooks.prototypeForTag;  function getTagFixed(o) {    var tag = getTag(o);    if (tag == "Document") {      // "Document", so we check for the xmlVersion property, which is the empty      if (!!o.xmlVersion) return "!Document";      return "!HTMLDocument";    }    return tag;  }  function prototypeForTagFixed(tag) {    if (tag == "Document") return null;    return prototypeForTag(tag);  }  hooks.getTag = getTagFixed;  hooks.prototypeForTag = prototypeForTagFixed;}
C.w=function(hooks) {  var userAgent = typeof navigator == "object" ? navigator.userAgent : "";  if (userAgent.indexOf("Firefox") == -1) return hooks;  var getTag = hooks.getTag;  var quickMap = {    "BeforeUnloadEvent": "Event",    "DataTransfer": "Clipboard",    "GeoGeolocation": "Geolocation",    "Location": "!Location",    "WorkerMessageEvent": "MessageEvent",    "XMLDocument": "!Document"};  function getTagFirefox(o) {    var tag = getTag(o);    return quickMap[tag] || tag;  }  hooks.getTag = getTagFirefox;}
C.x=function(hooks) {  var userAgent = typeof navigator == "object" ? navigator.userAgent : "";  if (userAgent.indexOf("Trident/") == -1) return hooks;  var getTag = hooks.getTag;  var quickMap = {    "BeforeUnloadEvent": "Event",    "DataTransfer": "Clipboard",    "HTMLDDElement": "HTMLElement",    "HTMLDTElement": "HTMLElement",    "HTMLPhraseElement": "HTMLElement",    "Position": "Geoposition"  };  function getTagIE(o) {    var tag = getTag(o);    var newTag = quickMap[tag];    if (newTag) return newTag;    if (tag == "Object") {      if (window.DataView && (o instanceof window.DataView)) return "DataView";    }    return tag;  }  function prototypeForTagIE(tag) {    var constructor = window[tag];    if (constructor == null) return null;    return constructor.prototype;  }  hooks.getTag = getTagIE;  hooks.prototypeForTag = prototypeForTagIE;}
C.k=function getTagFallback(o) {  var constructor = o.constructor;  if (typeof constructor == "function") {    var name = constructor.name;    if (typeof name == "string" &&        // constructor name does not 'stick'.  The shortest real DOM object        name.length > 2 &&        // On Firefox we often get "Object" as the constructor name, even for        name !== "Object" &&        name !== "Function.prototype") {      return name;    }  }  var s = Object.prototype.toString.call(o);  return s.substring(8, s.length - 1);}
C.y=function(getTagFallback) {  return function(hooks) {    if (typeof navigator != "object") return hooks;    var ua = navigator.userAgent;    if (ua.indexOf("DumpRenderTree") >= 0) return hooks;    if (ua.indexOf("Chrome") >= 0) {      function confirm(p) {        return typeof window == "object" && window[p] && window[p].name == p;      }      if (confirm("Window") && confirm("HTMLElement")) return hooks;    }    hooks.getTag = getTagFallback;  };}
$.ci="$cachedFunction"
$.cj="$cachedInvocation"
$.J=0
$.aa=null
$.bO=null
$.bz=null
$.cT=null
$.d6=null
$.aT=null
$.aV=null
$.bB=null
$.a2=null
$.ah=null
$.ai=null
$.bs=!1
$.m=C.b
$.bZ=0
$.aS=null
$.dc=null
$.h=null
$.D=null
$.aX=null
$.aR=null
$.bw=0
$.d2=0
$=null
init.isHunkLoaded=function(a){return!!$dart_deferred_initializers$[a]}
init.deferredInitialized=new Object(null)
init.isHunkInitialized=function(a){return init.deferredInitialized[a]}
init.initializeLoadedHunk=function(a){$dart_deferred_initializers$[a]($globals$,$)
init.deferredInitialized[a]=true}
init.deferredLibraryUris={}
init.deferredLibraryHashes={};(function(a){for(var z=0;z<a.length;){var y=a[z++]
var x=a[z++]
var w=a[z++]
I.$lazy(y,x,w)}})(["bS","$get$bS",function(){return init.getIsolateTag("_$dart_dartClosure")},"c2","$get$c2",function(){return H.e4()},"c3","$get$c3",function(){if(typeof WeakMap=="function")var z=new WeakMap()
else{z=$.bZ
$.bZ=z+1
z="expando$key$"+z}return new P.dX(null,z)},"cr","$get$cr",function(){return H.K(H.aL({
toString:function(){return"$receiver$"}}))},"cs","$get$cs",function(){return H.K(H.aL({$method$:null,
toString:function(){return"$receiver$"}}))},"ct","$get$ct",function(){return H.K(H.aL(null))},"cu","$get$cu",function(){return H.K(function(){var $argumentsExpr$='$arguments$'
try{null.$method$($argumentsExpr$)}catch(z){return z.message}}())},"cy","$get$cy",function(){return H.K(H.aL(void 0))},"cz","$get$cz",function(){return H.K(function(){var $argumentsExpr$='$arguments$'
try{(void 0).$method$($argumentsExpr$)}catch(z){return z.message}}())},"cw","$get$cw",function(){return H.K(H.cx(null))},"cv","$get$cv",function(){return H.K(function(){try{null.$method$}catch(z){return z.message}}())},"cB","$get$cB",function(){return H.K(H.cx(void 0))},"cA","$get$cA",function(){return H.K(function(){try{(void 0).$method$}catch(z){return z.message}}())},"bl","$get$bl",function(){return P.eX()},"aj","$get$aj",function(){return[]}])
I=I.$finishIsolateConstructor(I)
$=new I()
init.metadata=[null]
init.types=[{func:1},{func:1,v:true},{func:1,args:[,]},{func:1,v:true,args:[{func:1,v:true}]},{func:1,args:[,,]},{func:1,ret:P.M,args:[P.p]},{func:1,args:[,P.M]},{func:1,args:[P.M]},{func:1,args:[{func:1,v:true}]},{func:1,v:true,args:[,],opt:[P.a0]},{func:1,args:[,],opt:[,]},{func:1,args:[,P.a0]},{func:1,v:true,args:[,P.a0]},{func:1,args:[P.M,,]},{func:1,v:true,args:[,]},{func:1,v:true,args:[W.X]}]
function convertToFastObject(a){function MyClass(){}MyClass.prototype=a
new MyClass()
return a}function convertToSlowObject(a){a.__MAGIC_SLOW_PROPERTY=1
delete a.__MAGIC_SLOW_PROPERTY
return a}A=convertToFastObject(A)
B=convertToFastObject(B)
C=convertToFastObject(C)
D=convertToFastObject(D)
E=convertToFastObject(E)
F=convertToFastObject(F)
G=convertToFastObject(G)
H=convertToFastObject(H)
J=convertToFastObject(J)
K=convertToFastObject(K)
L=convertToFastObject(L)
M=convertToFastObject(M)
N=convertToFastObject(N)
O=convertToFastObject(O)
P=convertToFastObject(P)
Q=convertToFastObject(Q)
R=convertToFastObject(R)
S=convertToFastObject(S)
T=convertToFastObject(T)
U=convertToFastObject(U)
V=convertToFastObject(V)
W=convertToFastObject(W)
X=convertToFastObject(X)
Y=convertToFastObject(Y)
Z=convertToFastObject(Z)
function init(){I.p=Object.create(null)
init.allClasses=map()
init.getTypeFromName=function(a){return init.allClasses[a]}
init.interceptorsByTag=map()
init.leafTags=map()
init.finishedClasses=map()
I.$lazy=function(a,b,c,d,e){if(!init.lazies)init.lazies=Object.create(null)
init.lazies[a]=b
e=e||I.p
var z={}
var y={}
e[a]=z
e[b]=function(){var x=this[a]
try{if(x===z){this[a]=y
try{x=this[a]=c()}finally{if(x===z)this[a]=null}}else if(x===y)H.hr(d||a)
return x}finally{this[b]=function(){return this[a]}}}}
I.$finishIsolateConstructor=function(a){var z=a.p
function Isolate(){var y=Object.keys(z)
for(var x=0;x<y.length;x++){var w=y[x]
this[w]=z[w]}var v=init.lazies
var u=v?Object.keys(v):[]
for(var x=0;x<u.length;x++)this[v[u[x]]]=null
function ForceEfficientMap(){}ForceEfficientMap.prototype=this
new ForceEfficientMap()
for(var x=0;x<u.length;x++){var t=v[u[x]]
this[t]=z[t]}}Isolate.prototype=a.prototype
Isolate.prototype.constructor=Isolate
Isolate.p=z
Isolate.ak=a.ak
return Isolate}}!function(){var z=function(a){var t={}
t[a]=1
return Object.keys(convertToFastObject(t))[0]}
init.getIsolateTag=function(a){return z("___dart_"+a+init.isolateTag)}
var y="___dart_isolate_tags_"
var x=Object[y]||(Object[y]=Object.create(null))
var w="_ZxYxX"
for(var v=0;;v++){var u=z(w+"_"+v+"_")
if(!(u in x)){x[u]=1
init.isolateTag=u
break}}init.dispatchPropertyName=init.getIsolateTag("dispatch_record")}();(function(a){if(typeof document==="undefined"){a(null)
return}if(typeof document.currentScript!='undefined'){a(document.currentScript)
return}var z=document.scripts
function onLoad(b){for(var x=0;x<z.length;++x)z[x].removeEventListener("load",onLoad,false)
a(b.target)}for(var y=0;y<z.length;++y)z[y].addEventListener("load",onLoad,false)})(function(a){init.currentScript=a
if(typeof dartMainRunner==="function")dartMainRunner(function(b){H.d9(F.d4(),b)},[])
else (function(b){H.d9(F.d4(),b)})([])})})()